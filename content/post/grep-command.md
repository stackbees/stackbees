---
title: Grep command to find your files
date: 2018-03-09
tags: ["Linux", "System-Administration", "Unix" ]
categories:  [ "Linux" ]
---
<!-- TOC -->

- [Preface](#preface)
- [grep command syntax](#grep-command-syntax)
- [redirect output using pipe](#redirect-output-using-pipe)
- [Regex patterns using -E](#regex-patterns-using--e)

<!-- /TOC -->
### Preface
One of the most useful and versatile commands in a Linux terminal environment is the "grep" command. The name "grep" stands for "global regular expression print". This means that grep can be used to see if the input it receives matches a specified pattern. This seemingly trivial program is extremely powerful when used correctly. Its ability to sort input based on complex rules makes it a popular link in many command chains.


### grep command syntax

```
grep 'wiki' /etc/passwd
wiki:x:1000:1000:wiki,,,:/home/wiki:/bin/bash
```
###  redirect output using pipe

```
cat  /var/log/syslog | grep 'CRON'
wiki-thinkpad CRON[27599]: (root) CMD (   cd / && run-parts --report /etc/cron.hourly)
wiki-thinkpad CRON[29129]: (root) CMD (   cd / && run-parts --report /etc/cron.hourly)
wiki-thinkpad CRON[30800]: (root) CMD (   cd / && run-parts --report /etc/cron.hourly)
```
### Regex patterns using -E

```
grep -E "[[:alpha:]]{16,20}" /etc/ssh/sshd_config
```