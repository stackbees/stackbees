---
title: Understanding CPU Stats
date: 2022-07-24
tags: ["Linux", "System-Administration", "Tips&Tricks"  ]
categories:  [ "Linux" ]
---

Your Linux box is slow and you are running `top` command. Well, you are seeing a lot of metrics, what does these values means. 

## Major CPU Status

* **Idle**, the percentage of idle CPU
* **Running a user space program**, a User application running. It could be a compiler, Email client
* **Running the kernel**, Handling an interrupts, managing resources

The above said meta states can be divided. User space programs can be categorized as those running under their initial priority level or those running with a nice priority.

## The 7 CPU Statistics

```
Cpu(s):  0.3 us,  0.2 sy,  0.0 ni, 99.5 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st

```
<span style="color:blue">0.3 us</span> : This tells us that the processor is spending 0.3% of its time running **user space processes**.  A user space programe is any process that doesnt belongs to Kernel. It could be a database or a compiler or an email client. 

<span style="color:green">99.5 id</span> : 99% of your CPU are in Idle state during the last sampling period. The sum of **us** + **ni** + **id** should be equal or close to ``100%``

<span style="color:red">0.2 sy</span> : This is the amount of time that the CPU spent running the kernel. Each and every process and system resources are handled by the Linux kernel. When a user space process needs something from the system, for example when it needs to allocate memory, perform some I/O, or it needs to create a child process, then the kernel is running. The amount of time spent in the kernel should be as low as possible. In current case it is very low. 

<span style="color:yellow">0.0 ni</span> : Priority of a user process can be changed by tweaking its nicevalue. This value show how much time the CPU spent running user space processes that have been niced. If there is no process have been niced, then the value will be `0`.

<span style="color:black">0.0 wa</span> : IOWAIT percentage. When a process or program requests some data, it first checks the processor caches (there are 2 or three caches there), then goes out and checks memory, and finally will hit disk. When it hits disk, it generally has to wait for the IO thread to finish reading the information into RAM before it can operate on it again. The slower the disk, the higher the IO Wait % will be for each process.

<span style="color:blue">0.0 hi</span> : Hard Interrupt, when a network card receives a packet, before transferring the information in the packet to the processor via the kernel, it will ask for an interrupt on an interrupt line on the motherboard. A high “Hard Interrupt” stat is fairly rare on a virtual machine, but as hypervisors expose more “metal” to the VMs this may change. 

<span style="color:red">0.0 si</span> : Soft Interrupt, CPU time spent servicing/handling software interrupts. Software interrupts come from processes running on the system. A software interrupt doesn't occur at the CPU level, but rather at the kernel level.

<span style="color:green">0.0 st</span> : This applies to virtual workloads. When Linux is running as a virtual machine on a hypervisor, the **st** (short for stolen) statistic shows  the percentage of time the virtual machine process is waiting on the physical CPU for its CPU time.s








