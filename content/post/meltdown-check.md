---
title: How to Check Meltdown CPU Vulnerability in Linux
date: 2018-03-09
tags: ["Linux", "System-Administration", "Unix" ]
categories:  [ "Linux" ]
---
Meltdown is a chip-level security vulnerability that breaks the most fundamental isolation between user programs and the operating system. It allows a program to access the operating system kernel’s and other programs’ private memory areas, and possibly steal sensitive data, such as passwords, crypto-keys and other secrets. 

spectre-meltdown-checker is a simple shell script to check if your Linux system is vulnerable against the 3 “speculative execution” CVEs (Common Vulnerabilities and Exposures) that were made public early this year. Once you run it, it will inspect your currently running kernel.

```

$ git clone https://github.com/speed47/spectre-meltdown-checker.git 
$ cd spectre-meltdown-checker/
$ sudo ./spectre-meltdown-checker.sh


```
#### Output

{{< figure src="/img/meltdown-check.png" title="Meltdown Vulnerability" >}} </center>

### Futher reading