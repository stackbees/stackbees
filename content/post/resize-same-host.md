---
title: How to force openstack to resize on the same host
date: 2019-04-05
tags: ["Openstack", "Nova", "Devops" ]
categories:  [ "Linux" ]
---

I want to force openstack to resize every instance on the same host if it is possible to do. How can I do that?
Before that, why should I do that. Here, you have a use case, you are on a private cloud and running some critical
virtual machines in a very limited resources. You should take a turn to do this.

Add the following lines in ```/etc/nova/nova.conf```

```
allow_resize_to_same_host=True
scheduler_default_filters=AllHostsFilter
```

Restart Services
```
service nova-scheduler restart
service nova-compute restart
```