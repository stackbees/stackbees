---
title: How to use Head Command
date: 2018-03-09
tags: ["Linux", "System-Administration", "Unix" ]
categories:  [ "Linux" ]
---
<!-- TOC -->

- [Preface](#preface)
- [Head basic usage](#head-basic-usage)
- [n number of lines](#n-number-of-lines)
- [Specific number of bytes only](#specific-number-of-bytes-only)

<!-- /TOC -->
### Preface
There are several commands and programs provided by Linux for viewing the contents of file. Working with files is one of the daunting task, most of the computer users be it newbie, regular user, advanced user, developer, admin, etc performs. Working with files effectively and efficiently is an art. Today, in this article we will be discussing the most popular command called head.

### Head basic usage

```
head /var/log/auth.log
wiki-thinkpad CRON[17008]: pam_unix(cron:session): session opened for user root by (uid=0)
```
### n number of lines

```
head -n 50 /var/log/auth.log

It will print you the first 50 lines of auth.log file
```
### Specific number of bytes only

```
head -c 1000 /var/log/auth.log
This will print the first 1000 bytes of the file to your screen, and can be useful in situations where a file must be broken into pieces of a fixed size
```