---
title: Creating a Kali Linux Image for Openstack
date: 2018-05-06
tags: ["Linux", "Opensource", "General" ]
categories:  [ "Opensource" ]
---
<!-- TOC -->

- [Install required softwares](#install-required-softwares)
- [Get the Image](#get-the-image)
- [Unarchive](#unarchive)
- [Concatenate all the VMDK files to single QCOW2](#concatenate-all-the-vmdk-files-to-single-qcow2)
- [Upload to Glance](#upload-to-glance)

<!-- /TOC -->


Kali Linux is a Debian-derived Linux distribution designed for digital forensics and penetration testing. It is maintained and funded by Offensive Security Ltd. Mati Aharoni, Devon Kearns and Raphaël Hertzog are the core developers. Now a days lots of people using it for Security assessment. Here we will see how to create a QCOW2 image of Kali for Openstack.

## Install required softwares

```
sudo aptitude install qemu-kvm libvirt-bin ubuntu-vm-builder bridge-utils virt-goodies p7zip-full
```

## Get the Image

```
wget https://images.offensive-security.com/virtual-images/Kali-Linux-2016.1-vm-amd64.7z
```

## Unarchive

```
7z x Kali-Linux-2016.1-vm-amd64.7z
```

## Concatenate all the VMDK files to single QCOW2

```
sudo qemu-img convert -O qcow2 Kali-Linux-2016.1-vm-amd64/Kali-Linux-2016.1-vm-amd64-s00*.vmdk kali-rolling.qcow2
```

## Upload to Glance

```
glance image-create –name centos63-image –disk-format qcow2 –container-format bare –is-public True –file ./kali-rolling.qcow2
```