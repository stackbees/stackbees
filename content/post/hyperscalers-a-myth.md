---
title: Hyperscalers, Few Myths. 
date: 2022-05-04
tags: ["Cloud", "Virtualization", "Information Technology"  ]
categories:  [ "Linux" ]
---
<!-- TOC -->

- [What is a hyperscaler](#what-is-a-hyperscaler)
- [Do I need it ?](#do-i-need-it-)
- [Myths](#myths)
    - [Myth-01 Massive data centers are more efficient than smaller one.](#myth-01-massive-data-centers-are-more-efficient-than-smaller-one)
    - [Myth-02 Fanatic Support](#myth-02-fanatic-support)
    - [Myth-03 Hyperscalers are redundant and they have better Uptime](#myth-03-hyperscalers-are-redundant-and-they-have-better-uptime)
    - [Myth-04 Flexibility](#myth-04-flexibility)
    - [Myth-05 Large product portfolio](#myth-05-large-product-portfolio)

<!-- /TOC -->

### What is a hyperscaler

In computing, hyperscale is the ability of an architecture to scale appropriately as increased demand is added to the system. The cloud service provider should have ability to facilitate their infrastructure propotionate to market demands. This typically involves the ability to seamlessly provide and add compute, memory, networking, and storage resources to a given node or set of nodes. 

Benefits are multifolds. Recognize the power behind the service providers. Whether they can meet the demands of market. Of course, need for the hyperscaler is depends on the market they are operating. 

### Do I need it ?

One of the largest company, their virtual machine inventory never increased than 600 for the last 3 years. Its a leading in particular geographical area. Same like, in that particular area, customers care about the below points only. 

* Uptime
* Stability
* Support (Reachability to L3 round the clock)
* Support in Local language (L3 round the clock)
* Resolution speed

In this area, customers wont increase their workloads massively. Customers are moving to cloud, but these customers doesnt need so called Hyperscalers. Hyperscalers might be a good use case for the area, where people heavily depends on massive increase of their workloads. Big data / Streaming.

Hyperscalers are overrated Idea, at least some places where people care about above points than massive infrastructure. In these ares, hyperscalers are myths. Customers chose hyperscalers because of below 3 points

* 53% believe the alternative cloud providers outside the 'Big Three' are less secure.
* 43% believe they will suffer more outages.
* 32& believe they are more convenient

I would like to bring few points, which i figure it out based on my experience. These points are not applicable for global cloud computing industry. But of course its applicable in some geographical area. In this area, cloud computing is mandatory, but hyperscaler is not inevitable. 

### Myths
#### Myth-01 Massive data centers are more efficient than smaller one. 
Anything that is of enormous scale are complex and have specific set of problems to deal with. In reality, keeping it simple, small is more effective than giant one.

#### Myth-02 Fanatic Support
Support is customer perspective. Larger providers, assures full support for their customers. In papers, yes. But in reality, to get support you have to go through a weird support matrix. But in a particular country, I saw a CSP provides round the cloud L3 support to their customer. Regardless their size. I never saw such service from any provider, including one of the hyperscaler in the same market. 

Customer need to talk to a support personal for an urgent issue they are facing. They cannot wait for chat bot to answer some pre described text which doesnt have any relation with actual issue. Customer will be happy, when they are able to talk to a senior personal without escalation and more important is Local Language. 

#### Myth-03 Hyperscalers are redundant and they have better Uptime
Its another myth is, hyperscalers provides better uptime. [AWS](https://aws.amazon.com/) one of the hperscaler faced a serious [outage](https://web.archive.org/web/20220425231650/https://www.theguardian.com/technology/2021/dec/07/amazon-web-services-outage-hits-sites-and-apps-such-as-imdb-and-tinder) in 2021 which adversly affected [US-East-1](https://web.archive.org/web/20211207202444/https://punchng.com/alexa-tinder-disney-other-amazon-web-services-crash/) Region. Large customers like IMDB , Alexa and  Tinder were impacted. 

Downdetector showed more than 24,000 incidents of people reporting problems with Amazon. It tracks outages by collating status reports from a number of sources, including user-submitted errors on its platform.

One of the cloud provider in a middle east country, has an uptime of `600+` days. Its small compare to hyperscaler in the same market, but 100% uptime is remarkable while they offered to the customer is `99.995` only. 

#### Myth-04 Flexibility    
Small providers are more flexible than giants. Decision making will take more time in hyperscalers, but in small providers its a matter of minutes. People has decision making power even in root level. This will attract more customers, who really care about time consumption.

#### Myth-05 Large product portfolio
Hyperscalers has a very large product portfolio. Its good, like you are going to a super market. You can choose whatever you need. But, this is not applicable to all customer regions. Focust on what customer demanding. My point, do not go to a hyperscaler when you see a large shelve of products.  


