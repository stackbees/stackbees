---
title: Ten Directive Principles for choosing a Cloud Provider.
date: 2020-07-10
tags: ["Virtualization", "Cloudprovider", "Cloud"  ]
categories:  [ "Linux" ]
---
<!-- TOC -->

- [Cloud! Cloud! Cloud!](#cloud-cloud-cloud)
- [Reputation](#reputation)
- [Technology & Roadmap](#technology--roadmap)
- [Data governance and security](#data-governance-and-security)
- [Contracts & SLA](#contracts--sla)
- [Reliability & Performance](#reliability--performance)
- [Migration support](#migration-support)
- [Commercials & Discounts](#commercials--discounts)
- [Support](#support)
- [Exit planning](#exit-planning)

<!-- /TOC -->


## Cloud! Cloud! Cloud!
When everyone of your company stakeholders agrees to put all your applications on a cloud, next question is `How can I choose my cloud provider?`. Its an important thing while choosing a cloud provider. Not only, price there is few other things matters. Here I would like to mention few.

## Reputation
You should consider the proposed service provider reputation. How long he is in the industry. Whats their business area ?. Major partners with the company. Major investors in the company. Goodwill of the company really matters. A company doesnt have a good reputation, of course noone will choose him as his technology partner.

## Technology & Roadmap
You have to make sure that, the providers technology is aligned with your IT needs. Is the cloud providers technology suits your  workloads and management objectives.
You should consider the volume of customization you need to do with your current workloads to suits with providers environment. Most of the service providers offers migration services.

You have to ask to the service providers about their road map for at least next two years. You are not going to change your cloud provider at least another 5 years. So, roadmap is very important. Ask for a demostration if possible

## Data governance and security
`IAC` is important, I said Important. `I`ntegrity `A`vailability `C`onfidentiality. All i am talking about your data. Now a days, a lot of cloud security certifications available for service providers. You need to check the passed certifications for your proposed cloud provider. Most of the cloud providers will demostrate the certifications in their website. Some of the major certifications are listed below

* CSA Star
* TIER IV
* ISO 27001
* ISO 27018
* ISO 9001
* PCI DSS

*more certifications, more trust*

## Contracts & SLA
Cloud agreements are complex. Each company have their own, of course there will be some common components. Asking questions and understanding is your responsibility when you are dealing with SLA (Service Level Agreement). Read twice or thrice about the demarcation points.

## Reliability & Performance
Reliability is very important, because you have a customer spectrum to serve. Make the service available is your responsibility and it depends upon your service providers responsibility. You can ask for the Service Availability for the last one year, or you can check their status page for the last one year outage. If the provider doesnt have at least 99.9999 % uptime, OK better look for someone who provide this.

Performance is another criteria. But performance depends up on multiple factors. For example, when you are talking about DISK IOPS, you need to worry about IOPS while you are having a high sensitive SQL cluster. But, what about you are having Webserver cluster, your concerns are about bandwidth and availability of the cluster. So, it depends on your workloads. Every cloud provider in the market clearly demostrate their performance benchmarks.

## Migration support
Well, you are migration your workloads from baremetal to cloud. You need a partner to do this job. May be your current environment is running on different technology stack but you are going to migrating to a different technology. Check with your proposed provider, whether they offers migration support. Most of the cloud providers offers migration tools to bring customer workloads to their cloud platform. Afterall its their business. 

## Commercials & Discounts
Each cloud providers have unique pricing strategy, which will be available in their marketplace. Most of the pricing strategy they demostrated is without Discounts. You are the end user, its your right to ask for discounts. I promise, there will be discounts. Some providers will give mind blowing discounts, but you have to agree with them for a certain term of commitment. Even thats fine, you are looking for a long term relationship.

## Support
Getting support is very important. Consider, you are having a severit 1 issue with your workloads and you call the helpline number and someone answer to your concern in local language. Believe me, its really important. Most of the cloud provider give support in multiple languages. But, sure some proactive companies providers give support in your local language.

You should be clearly understand the escalation matrix in SLA. 

## Exit planning 
You should have a clear exit strategy before starting your relationship with your cloud provider. It seems funny, but thats the truth. For any reason, if you want to leave the cloud provider you should not be locked in. You should have a written confirmation from the cloud provider about the exit strategy like, Data export, Cancelling the subscription, Purge the data in their storage.








