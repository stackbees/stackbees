---
title: Live Migration failure Cannot get interface MTU on ‘qbrXXXXXXXX-XX’
date: 2018-05-06
tags: ["Openstack", "Opensource", "General" ]
categories:  [ "Opensource" ]
---

Some times, when you are doing a live migration , it will ended up with the following error in the nova-compute.log of hypervisor. The reason for this as follows.
The Interface was removed by the user, but LIBVIRT doesnt acknowledged about this. But nova did. So, when nova initiates a live migration, libvirt is looking for this
interface, which actually doesnt exists.

```virsh domiflist```

| Interface      | Type    | Source         | Model  | MAC               |
|----------------|---------|----------------|--------|-------------------|
| tap29b1bcde-c7 | bridge  | qbr29b1bcde-c7 | virtio | fa:16:3e:79:06:2a |
| tap887baa4b-2e | bridge  | qbr887baa4b-2e | virtio | fa:16:3e:d8:e9:db |

At the same time

```nova interface-list 41d0b857-82a4-495b-9a91-b62ec8a879c8```

| Port State | Port ID                              | Net ID                                | IP addresses | MAC Addr          |
|------------|--------------------------------------|---------------------------------------|--------------|-------------------|
| ACTIVE     | 3de64520-f085-4b17-ad5e-5114a6bcf219 | 4f47a8bc-5334-47b8-adcc-49d8f0b6f13d  | 10.10.10.3   | 02:3d:e6:45:20:f0 |


The interface ```tap29b1bcde-c7``` is not exist. During the live-migration, libvirt looking for the interface and could not find. He reports to nova that, he could not find the interface and aborting live-migration. To solve this, we need to remove the not used interface. Use the following command to do this.

```virsh detach-interface --domain instance-0000bab1 --type bridge --mac fa:16:3e:79:06:2a --config --live```

Make sure that, interface Doesnt exists and run the live-migration. It will be succeeded.