---
title: Password Authenticaion, Cloud Images
date: 2019-04-05
tags: ["Openstack", "Glance", "qcow2" ]
categories:  [ "Linux" ]
---
<!-- TOC -->

- [Use case](#use-case)
- [login using the pem key](#login-using-the-pem-key)
- [Elevate your privileges](#elevate-your-privileges)
- [Create a new user](#create-a-new-user)
- [Set the password for the new user](#set-the-password-for-the-new-user)
- [Restart SSH Service](#restart-ssh-service)

<!-- /TOC -->
## Use case
The cloud images bundled by various linux distributions have password authentication disabled by default for security reasons. The only possible way to login to an instance launched using one of these images is by specifying a security key during boot and using the key to ssh. Often you would want to enable password authentication like say to login through VNC console for debugging. This post will take you through the steps involved for enabling password authentication.

## login using the pem key
```
ssh -i key_pair-name.pem user@floatingip 
```
`You should create key-pair before you boot any instance using it. On an openstack setup you can create key-pairs under`
```
Project-> Compute-> Access&Security-> Key Pairs
```
As soon as you create one you would be prompted to download and save it to your disk. Later you can use the `key-pair-name.pem` file to ssh into your instance.
Depending on the distribution of your image you have a default user to login as. For example on an Ubuntu image the default user is `ubuntu`. For Centos, the default user is `centos`. You can replace the floatingip with the FloatingIP address of your instance.
## Elevate your privileges
```
sudo su -
```
## Create a new user
```
useradd -G sudo -U -m username
```
Replace ‘username’ with the name of your choice. The above command adds the newly created user to the ‘sudo’ group. This is necessary because in most cloud images all members of ‘sudo’ group have password-less ‘sudo’ access. Without this you might not be able to elevate your privileges after login. On certain systems that do not have ‘sudo’ group ‘admin’ group will have ‘sudo’ access and you can replace ‘sudo’ with ‘admin’ in the above command.

## Set the password for the new user
```
passwd username
```
Enter the new password for the user and confirm the same.

Open `/etc/ssh/sshd_config` and set the following parameters

```
ChallengeResponseAuthentication yes
PasswordAuthentication yes

```
`Additionally if you want to enable root login you can set the below param too`
```
PermitRootLogin yes
```
`Then set a password for root user too`
```
passwd root
```
and enter the new password for root user and confirm the same.

## Restart SSH Service
```
service ssh restart    (For Ubuntu Servers)
```
```
service sshd restart  (For Centos Servers)
```
