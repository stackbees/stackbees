---
title: Create QCOW2 image for Openstack
date: 2019-04-05
tags: ["Openstack", "Glance", "qcow2" ]
categories:  [ "Linux" ]
---



## Introduction
Creating a new image is a step done outside of your ```OpenStack``` installation. You create the new image manually on your own system and then upload the image to your cloud.

To create a new image, you will need the installation CD or DVD ISO file for the guest operating system. You will also need access to a virtualization tool. You can use KVM for this. Or, if you have a GUI desktop virtualization tool (such as, [VMware Fusion](https://www.vmware.com/mena/products/fusion.html) or [VirtualBox](https://www.virtualbox.org/wiki/Downloads)), you can use that instead. Convert the file to raw once you are done.

## Verify the Network
```
# virsh net-list

Name                 State      Autostart
-----------------------------------------
default              active     yes
```
*If its not active, start the network
```
virsh net-start default
```

## Use the virt-manager X11 GUI
```
$ ssh -X root@server virt-manager
```
```
$ ssh -X user@server
$ sudo virt-manager
```
Click the ```Create``` a new virtual machine button at the top-left, or go to *File ‣ New Virtual Machine*. Then, follow the instructions.
![](https://kb.bluvalt.com/uploads/virt-manager.png)

*Follow the onscreen instructions to complete the Installation*

*Once the Virtual Machine is up and running, Login using Credentials*


## Install Cloud Init
```
sudo apt-get install cloud-init

```

## Configure the Cloud init
```
dpkg-reconfigure cloud-init
```
*The account varies by distribution. On Ubuntu-based virtual machines, the account is called ubuntu. On Fedora-based virtual machines, the account is called ec2-user. You can change the name of the account used by cloud-init by editing the /etc/cloud/cloud.cfg file and adding a line with a different user. For example, to configure cloud-init to put the key in an account named admin, use the following syntax in the configuration file*
```
users:
  - name: admin
    (...)
```

## Shutdown the Virtual Machine
```
 /sbin/shutdown -h now
```

## Clean up (remove MAC address details)
```
virt-sysprep -d trusty
```

*The operating system records the MAC address of the virtual Ethernet card in locations such as /etc/udev/rules.d/70-persistent-net.rules during the installation process. However, each time the image boots up, the virtual Ethernet card will have a different MAC address, so this information must be deleted from the configuration file. There is a utility called virt-sysprep, that performs various cleanup tasks such as removing the MAC address references. It will clean up a virtual machine image in place*

## Undefine the libvirt domain
```
virsh undefine trusty
```

## Upload to Your project
```
openstack image create --disk-format qcow2 --container-format bare  --public --file ./centos63.qcow2 centos63-image
```