---
title: Customer Support is an Art, not a Job !
date: 2020-07-17
tags: ["Customer", "Customerexperience", "Customersupport"  ]
categories:  [ "Linux" ]
---


*`A customer is the most important visitor on our premises. He is not dependent on us. We are dependent on him. He is not an interruption of our work. He is the purpose of it. He is not an outsider of our business. He is part of it. We are not doing him a favour by serving him. He is doing us a favour by giving us the opportunity to do so.`*
--**Mohandas Karamchand Gandhi**


<!-- TOC -->

- [Customer is the Queen](#customer-is-the-queen)
- [Responsibility](#responsibility)
- [Promise](#promise)
- [Simplicity](#simplicity)
- [Adaptation](#adaptation)
- [Availability](#availability)
- [Bond](#bond)
- [Proactive](#proactive)

<!-- /TOC -->


## Customer is the Queen
He is the one who providing business to you. You cannot make business without him. If you think, you make business by convincing him, brainwashing him I would say `You are completely wrong`. He chooses you. Some times, you may feel the customer behaving like a layman. Yes, then explain to him as a 6th grade student. For the customer, there is multiple service providers outside. For you, he is the only one. 

## Responsibility
Taking responsibility is very important in customer experiencing. Sometimes, customer will come to you ask for a different component which you are not expert. Never tell the customer, its not my job, you look for right people. You should take the responsibility and tell the customer, I am not the right person, but I will communicate with the right team and get back to you as soon as possible you can. 

## Promise
`Keep the promise`. If you agreed to provide a call back / update / response in a particular date or time, make sure that you contacted him. Even you dont have an answer for that query, just inform in that time. Broken promises will lead to broken relationships. Never do that.

## Simplicity
Make things simple for the customer. Never give a lot of options for his requirement. Understand what he need and provide an Optimum, Feasible, Secure solution. 
Solution should be suitable and fit for his wallet as well. I am personally against creating a solution for his budget, create a solution optimal for him. It may be half of his budget, but provide it. I saw many companies, ask for the budget and provide a solution exactly same for the budget. 

## Adaptation
Identify the customer through the `MAN` theory.
* `M`oney
* `A`uthority
* `N`eed

You may not talk with the right person in the customer premises, even he is pretending. Identify the person who can take decision on Money matters, who is having clear Authority to take decision and finally one who really knows the exact requirement.

## Availability
Our customers expect to contact us on multiple channels, Email , Phone, Chat etc. They expect their questions to be answered in timely manner and it should have accuracy. They dont want to answer your questions repeatedly. L1 asks the questions, L2 asks the same question in a different way, thats not acceptable. Make sure, you gather all the informations you needed in the first place.

## Bond
Develop a bond between you and your customer. Damn sure, he will be redirect to you a worthy deal. Even your customers doesnt have a problem, call them and check about the services you provided. Whether he need an enhancement, whether he need a support for anything. It will make him happy, and he will definitely refer you.

## Proactive
Communicate with the customer when there is an issue happened in the service you provided to him. Do not wait for the customer to acknowledge you about the issue.
Inform him and provide him an expected time to resolve the issue if you have one. If you dont have an ETA, do not provide false information. Because he is a customer of us, same like he is having customers to serve.





