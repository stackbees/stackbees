---
title: Overcommit per Aggregate in Openstack
date: 2019-11-02
tags: ["Linux", "System-Administration", "Openstack", "Compute"  ]
categories:  [ "Openstack" ]
---
<!-- TOC -->

- [What is an Overcommit in cloud ?](#what-is-an-overcommit-in-cloud-)
    - [Overcommitting Memory](#overcommitting-memory)
    - [Overcommitting CPU](#overcommitting-cpu)
- [Why its important in a Public Cloud](#why-its-important-in-a-public-cloud)
- [Where are the Overcommit values in Openstack](#where-are-the-overcommit-values-in-openstack)
- [Well, I have a very special requirement](#well-i-have-a-very-special-requirement)
- [Solution](#solution)
    - [Create the aggregate](#create-the-aggregate)
    - [Add your computes to this Aggregate](#add-your-computes-to-this-aggregate)
    - [Setting up Overcommit values to this particular Aggregate](#setting-up-overcommit-values-to-this-particular-aggregate)
    - [How the Guest VM know, i should go to this Aggregate](#how-the-guest-vm-know-i-should-go-to-this-aggregate)
    - [Inject Flavor metadata](#inject-flavor-metadata)
    - [Check the flavor now](#check-the-flavor-now)
    - [Add few filters in nova](#add-few-filters-in-nova)
    - [Disable the current allocation from nova](#disable-the-current-allocation-from-nova)
    - [Restart nova](#restart-nova)
- [Workflow](#workflow)
- [Further Reading](#further-reading)

<!-- /TOC -->

## What is an Overcommit in cloud ?

### Overcommitting Memory

Most operating systems and applications do not use 100% of the available RAM all the time. This behavior can be utilized with hypervisor to use more memory for virtualized Guest Operating Syste than what is physically available. At the end virtual machines are Linux processes. Virtual machines do not have blocks of physical RAM assigned to them instead they function as processes. Each process is allocated memory when it requests more memory. hypervisor uses this to allocate memory for guests when the guest operating system requests more or less memory through a `virtio` corridor. 

### Overcommitting CPU

Most of the  hypervisors supports overcommitting virtualized CPUs. Virtualized CPUs can be overcommitted as far as load limits of virtualized guests allow. Use caution when overcommitting VCPUs as loads near 100% may cause dropped requests or unusable response times. Virtualized CPUs are overcommitted best when each virtualized guest only has a single VCPU. The Linux scheduler is very efficient with this type of load. Hypervisor should safely support guests with loads under 100% at a ratio of five VCPUs. Overcommitting single VCPU virtualized guests is not an issue.

## Why its important in a Public Cloud

A public cloud hosting multiple customers, multiple projects. Consider this example, a compute host have 2 Sockets and each sockets have 24 Cores. While we doing a hyperthreading, you will get a 96 `pCPU` to use. If this cloud have 10 compute hosts same like configuration you can have 960 pCPU. And it can host, 960 virtual machines with a Single `vCPU` , so whats the `RTO`. You cannot reach your breakeven near future. So, we will increase the number of vCPU using overcommitting. The value can be any number based on the Business needs.

## Where are the Overcommit values in Openstack

Normally you can find these values in `/etc/nova/nova.conf` file of Controller nodes.
```
cpu_allocation_ratio= 16.0
ram_allocation_ratio = 1.5
```
What does it mean?. Well, consider the above example with a 96 `pCPU`, by doing the `16` overcommit you will get `1500` vCPU. `Alert` This is an aggressive value. You can host `1500` Guest Os with a Single `vCPU`. Same like, RAM if you have 100GB physical ram, it will be raised to `100*1.5` which is `150`. This is a configurable values and will be applied to all the compute hosts in the cluster

## Well, I have a very special requirement

I am running a public cloud. And I have 100 compute hosts, 25 of them with 1Terabyte of RAM and 96 CPU. I need a very special overcommit values for these hosts, because its hosting very sensitive workloads. I need a `cpu_allocation = 8` and `ram_allocation = 1  ` , how can I achieve this.

## Solution
You need to use the Aggregate concept of Openstack. First create an aggregate called `memorty_ops`

### Create the aggregate
```
[root@controller ~]# nova aggregate-create memory_ops Ezone-IND
+----+------------+-------------------+-------+-------------------------------+
| Id | Name       | Availability Zone | Hosts | Metadata                      |
+----+------------+-------------------+-------+-------------------------------+
| 3  | memory_ops | Ezone-IND         |       | 'availability_zone=Ezone-IND' |
+----+------------+-------------------+-------+-------------------------------+
```
### Add your computes to this Aggregate
```
[root@controller ~]# nova aggregate-add-host 3 compute1
Host compute1 has been successfully added for aggregate 3 
+----+------------+-------------------+------------+-------------------------------+
| Id | Name       | Availability Zone | Hosts      | Metadata                      |
+----+------------+-------------------+------------+-------------------------------+
| 3  | memory_ops | Ezone-IND         | 'compute1' | 'availability_zone=Ezone-IND' |
+----+------------+-------------------+------------+-------------------------------+
+----+------------+-------------------+------------+-------------------------------+
| 3  | memory_ops | Ezone-IND         | 'compute2' | 'availability_zone=Ezone-IND' |
+----+------------+-------------------+------------+-------------------------------+
+----+------------+-------------------+-------------+-------------------------------+
| 3  | memory_ops | Ezone-IND         | 'compute25' | 'availability_zone=Ezone-IND' |
+----+------------+-------------------+-------------+-------------------------------+
```
Add all your 25 compute host to these Aggregate

### Setting up Overcommit values to this particular Aggregate
```
nova aggregate-set-metadata 3 cpu_allocation_ratio=8.0
nova aggregate-set-metadata 3 ram_allocation_ratio=1.0
nova aggregate-set-metadata 3 host_type=memory
```
### How the Guest VM know, i should go to this Aggregate
Well you need some Flavor meta_data and need to inject to your Flavor. 

```
openstack flavor show 6484521-b53r-4f4d-8b9b-7f4a4c2bc5345
+----------------------------+-----------------------------------------------------------------+
| Field                      | Value                                                           |
+----------------------------+-----------------------------------------------------------------+
| OS-FLV-DISABLED:disabled   | False                                                           |
| OS-FLV-EXT-DATA:ephemeral  | 0                                                               |
| access_project_ids         | None                                                            |
| disk                       | 30                                                              |
| id                         | 6484521-b53r-4f4d-8b9b-7f4a4c2bc5345                            |
| name                       | M1-Memory-12                                                    |
| os-flavor-access:is_public | True                                                            |
| properties                 |                                                                 |
| ram                        | 16384                                                           |                                                                       
| rxtx_factor                | 1.0                                                             |                                                                       
| vcpus                      | 8                                                               |                                                                       
+----------------------------+-----------------------------------------------------------------+
```
### Inject Flavor metadata

```
openstack flavor set 6484521-b53r-4f4d-8b9b-7f4a4c2bc5345 --property aggregate_instance_extra_specs:host_type='memory'
```
### Check the flavor now
As you can see an new property added.
```
openstack flavor show 6484521-b53r-4f4d-8b9b-7f4a4c2bc5345
+----------------------------+-----------------------------------------------------------------+
| Field                      | Value                                                           |
+----------------------------+-----------------------------------------------------------------+
| OS-FLV-DISABLED:disabled   | False                                                           |
| OS-FLV-EXT-DATA:ephemeral  | 0                                                               |
| access_project_ids         | None                                                            |
| disk                       | 30                                                              |
| id                         | 6484521-b53r-4f4d-8b9b-7f4a4c2bc5345                            |
| name                       | M1-Memory-12                                                    |
| os-flavor-access:is_public | True                                                            |
| properties                 | aggregate_instance_extra_specs:host_type='memory'	           |
| ram                        | 16384                                                           |                                                                       
| rxtx_factor                | 1.0                                                             |                                                                       
| vcpus                      | 8                                                               |                                                                       
+----------------------------+-----------------------------------------------------------------+
```
### Add few filters in nova
Make sure that, the following filters are enabled in `/etc/nova/nova.conf`
```
enabled_filters=DifferentHostFilter,SameHostFilter,RetryFilter,AvailabilityZoneFilter,RamFilter,CoreFilter,DiskFilter,ComputeFilter,ComputeCapabilitiesFilter,ImagePropertiesFilter,ServerGroupAntiAffinityFilter,ServerGroupAffinityFilter,PciPassthroughFilter,NUMATopologyFilter,AggregateInstanceExtraSpecsFilter
```
### Disable the current allocation from nova
You need to disable the the current `allocation` in `/etc/nova/nova.conf`. Because we are not going to use the Global configuration
```
#cpu_allocation_ratio= 16.0
#ram_allocation_ratio = 1.5
```
### Restart nova
```
systemctl restart nova-compute.service
systemctl restart nova-scheduler.service
```
## Workflow

* User Creating the Virtual machine
* User choses the flavor `M1-Memory-12`
* `nova_scheduler` is checking an aggregate having a meta_data `memory`
* `nova_scheduler` find an aggregate (memory_ops) with concerned meta_data
* `nova` choose one of the compute host from 25 and Spawn the virtual machine there

## Further Reading
* [Nova Schedulers](https://docs.openstack.org/nova/latest/user/filter-scheduler.html)
* [Image Properties](https://docs.openstack.org/glance/rocky/admin/useful-image-properties.html)
* [Memory Overcommit](https://en.wikipedia.org/wiki/Memory_overcommitment)


