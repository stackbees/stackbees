---
title: Openstack Live migration, What? Why? How?
date: 2019-11-06
tags: ["Openstack", "nova", "cloud-operation" ]
categories:  [ "Linux" ]
---
<!-- TOC -->

- [What is Live Migration](#what-is-live-migration)
- [Why we need a Live Migration](#why-we-need-a-live-migration)
    - [Host Failure](#host-failure)
    - [Maintanance mode](#maintanance-mode)
    - [Optimal resource usage](#optimal-resource-usage)
- [How a Live migration happening ?](#how-a-live-migration-happening-)
    - [Pre-migration](#pre-migration)
    - [Reservation](#reservation)
    - [Iterative pre-copy](#iterative-pre-copy)
    - [Stop and Copy](#stop-and-copy)
    - [Commitment](#commitment)
- [Pitfalls](#pitfalls)
- [Abort ?](#abort-)
- [Futher reading](#futher-reading)

<!-- /TOC -->
## What is Live Migration
Live-migrating an instance means moving its virtual machine to a different OpenStack Compute server while the instance continues running. When you are operating a public cloud OR private cloud you need to do this for many reasons. During the live migration, users wont be aware or notified about his virtual machines are migrating from one host to another. During this process, instance wont be shutdown.

## Why we need a Live Migration

### Host Failure
The compute host which hosts guest virtual machines is failed. There is many reasons for this failure, such as cooling issues, Storage problems, Networking problems, Your datacenter stuck by a flood (Worst Scenario).

### Maintanance mode
You need to upgrade your firmware. You need to upgrade the hardware, like increase the memory stick. OR you need to upgrade your Operating system Kernel

### Optimal resource usage
Spread the guest virtual machines across the other compute host. Mitigate the noisy neighbour. Host more virtual machines on the compute host to save power. Help a VIP customer to deploy his virtual machine in a HA mode.

## How a Live migration happening ?
### Pre-migration
The `VM1` is residing on `compute-01` need to migrate to another host which is `compute-02`
```
openstack server migrate f3ef36e9-axvf-5fa45-878c-12345615bab2 --live compute-02
```
User commanding `nova-scheduler` to chose the host `compute-02` as the destination host. In this case, user Identify the host himself, and he confirm that the destination host have enough resource. If the User doesnt choose the host, `nova-scheduler` will choose the best,optimum hypervisor for you. 
### Reservation
Confirm the availability of resource in `compute-02`. Reserve the resouce for incoming guest `VM1`
### Iterative pre-copy
Memory is copied to `compute-02` from `compute-01`. After that dirtied pages are iteratively copied.
### Stop and Copy
Suspend the VM1 and copying remaining pages. Finally the CPU state. The `suspend` state doesnt mean, virtual machine is `DOWN`.
### Commitment
The `compute-02` will be the primary residence for `VM1`

## Pitfalls
* Guest virtual machines with intensive memory consumption are hard to workload
* Live migration will generate a heavy load on the Network
* Memory overconsumption
* Different `CPU` architecture in destination host will interrupt the Live migration
* NOVA wont allow any operation on the Guest VM during Live migration
* But you can use `virsh` command in compute host to interact. 

## Abort ?
You can abort the Live migration if needed.
```
nova migration-list | grep <vm-id>
```
```
nova live-migration-abort <server-id> <live-migraion-id>
```
## Futher reading
* [Openstack Documentation](https://docs.openstack.org/nova/pike/admin/live-migration-usage.html)
* [Live migration explained](https://raymii.org/s/articles/Openstack_-_(Manually)_migrating_(KVM)_Nova_Compute_Virtual_Machines.html)

