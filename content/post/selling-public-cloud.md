---
title: How We Successfully Sold Public Cloud 
date: 2023-06-27
tags: ["Linux", "Virtualization", "Cloud"  ]
categories:  [ "Linux" ]
---
<!-- TOC -->

- [Introduction:](#introduction)
- [Key points Not limited to this](#key-points-not-limited-to-this)
    - [Education and Training:](#education-and-training)
    - [Free Trials:](#free-trials)
    - [Architecting:](#architecting)
    - [Implementing:](#implementing)
    - [BOAT Built Operate and Transfer:](#boat-built-operate-and-transfer)
    - [Fanatic Support:](#fanatic-support)
    - [Price Discounts](#price-discounts)
- [Conclusion:](#conclusion)
    - [Further reading](#further-reading)

<!-- /TOC -->

## Introduction:
In recent years, public cloud computing has revolutionized the way organizations manage their IT infrastructure. It offers scalability, flexibility, cost-effectiveness, and numerous other benefits. However, selling public cloud solutions to businesses requires a strategic approach. In this blog post, we will explore the key factors that contributed to the successful sale of public cloud services, including education and training, free trials, architecting, implementing, BOAT (Built Operate and Transfer), and providing fanatic support.

In 2014, local market was not ready to adopt virtualization. The largest internet service provider entered in to the market with cloud product, the business was not that easy.  Major concern was data privacy, how can we trust a third party provider when we move our data to their servers. Another question was responsibility matrix. During that time, customers were still put their trust on baremetal servers. Even the scalability, procurement, operational cost is higher than compared to cloud, people were not ready to change their mind. 

## Key points (Not limited to this)

### Education and Training:
One of the initial steps to selling public cloud solutions is educating potential customers about the benefits and advantages it offers. This involves conducting training sessions, workshops, and seminars to enhance their understanding of cloud computing concepts, migration strategies, security measures, and cost-saving potential. By providing comprehensive education, businesses can alleviate concerns and build trust with customers, leading to successful sales.

### Free Trials:
Offering free trials of public cloud services has been an effective strategy to showcase the capabilities and value of the platform. Customers can explore the features, test workloads, and assess how the cloud solution aligns with their specific business needs. Free trials allow businesses to experience the benefits firsthand, fostering confidence and increasing the likelihood of a sale once the trial period ends.

### Architecting:
Successful public cloud sales involve working closely with customers to architect customized solutions. Understanding their unique requirements, challenges, and growth plans helps in designing cloud infrastructures tailored to their needs. This involves identifying the right cloud services, selecting appropriate providers, optimizing costs, and ensuring scalability. By providing expert guidance in architecting solutions, businesses can demonstrate their expertise and help customers make informed decisions.

### Implementing:
The implementation phase plays a crucial role in the successful adoption of public cloud solutions. By offering assistance during the migration process, including data transfer, application deployment, and system integration, businesses can mitigate challenges and minimize disruptions. This hands-on approach builds customer confidence, as they witness a smooth transition to the cloud with minimal downtime and improved efficiency.

### BOAT (Built Operate and Transfer):
BOAT, which stands for Built Operate and Transfer, is a model that has gained popularity in selling public cloud services. It allows businesses to provide a turnkey cloud solution, taking care of the entire setup, operations, and management on behalf of the customer. Over time, as customers gain confidence and familiarity with the cloud environment, the ownership of the infrastructure is transferred gradually. BOAT enables a seamless transition to the cloud, providing customers with the support they need while ensuring a smooth handover process.

### Fanatic Support:
To differentiate themselves in the competitive cloud market, businesses must provide exceptional customer support. This includes 24/7 availability, prompt response times, proactive monitoring, and ongoing assistance with troubleshooting and optimization. By offering fanatic support, businesses create a positive customer experience, build long-term relationships, and generate positive word-of-mouth, leading to increased sales and customer loyalty.

### Price Discounts
Price discounts play a significant role in selling public cloud services. Offering competitive pricing models and discounts can attract potential customers and incentivize them to choose a particular cloud provider. These discounts can be based on factors such as contract length, usage volume, or specific promotional offers. By providing cost savings through price discounts, businesses can enhance the appeal of their public cloud services and increase their chances of making successful sales.

## Conclusion:
Selling public cloud services requires a multifaceted approach that encompasses education, free trials, architecting, implementation, BOAT, and fanatic support. By focusing on these key areas, businesses can effectively address customer concerns, demonstrate the value of cloud solutions, and foster successful sales. As the demand for cloud computing continues to grow, companies that master these strategies will be well-positioned to thrive in the ever-evolving digital landscape.

### Further reading
* [An ISP journey to Public Cloud](https://stackbees.online/post/isp-journey-to-public-cloud/)