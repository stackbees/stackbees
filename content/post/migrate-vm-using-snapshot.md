---
title: Migrate instances using Snapshot
date: 2019-04-06
tags: ["Linux", "Openstack", "Nova"  ]
categories:  [ "Linux" ]
---
<!-- TOC -->

- [Preface](#preface)
- [Taking snapshot](#taking-snapshot)
- [Download the Image](#download-the-image)
- [Create Image in the Destination Project](#create-image-in-the-destination-project)
- [Boot the new Instance](#boot-the-new-instance)

<!-- /TOC -->
## Preface
Cloud Administrators, or End users need to migrate instances from one Porject to another. Or, a Cloud to another cloud. There is multiple ways to achieve this. The easiest way to use snapshot feature and do the migration.

* Shutdown the Virtual Machine

```
$ openstack server list
+--------------------------------------+------------+--------+------------------------------+------------+
| ID                                   | Name       | Status | Networks                     | Image Name |
+--------------------------------------+------------+--------+------------------------------+------------+
| c41f3074-c82a-4837-8673-fa7e9fea7e11 | myInstance | ACTIVE | private=10.0.0.3             | cirros     |
+--------------------------------------+------------+--------+------------------------------+------------+
```
```
openstack server stop myInstance
```
```
$ openstack server list
+--------------------------------------+------------+---------+------------------+------------+
| ID                                   | Name       | Status  | Networks         | Image Name |
+--------------------------------------+------------+---------+------------------+------------+
| c41f3074-c82a-4837-8673-fa7e9fea7e11 | myInstance | SHUTOFF | private=10.0.0.3 | cirros     |
+--------------------------------------+------------+---------+------------------+------------+
```
## Taking snapshot
```
$ openstack server image create myInstance --name myInstanceSnapshot
```

```
$ openstack image list
+--------------------------------------+---------------------------------+--------+
| ID                                   | Name                            | Status |
+--------------------------------------+---------------------------------+--------+
| 657ebb01-6fae-47dc-986a-e49c4dd8c433 | cirros-0.3.5-x86_64-uec         | active |
| 72074c6d-bf52-4a56-a61c-02a17bf3819b | cirros-0.3.5-x86_64-uec-kernel  | active |
| 3c5e5f06-637b-413e-90f6-ca7ed015ec9e | cirros-0.3.5-x86_64-uec-ramdisk | active |
| f30b204e-1ce6-40e7-b8d9-b353d4d84e7d | myInstanceSnapshot              | active |
+--------------------------------------+---------------------------------+--------+
```
## Download the Image
```
openstack image save --file snapshot.raw f30b204e-1ce6-40e7-b8d9-b353d4d84e7d
```
<span style="background-color:grey">The openstack image save command requires the image ID or the image name. Check there is sufficient space on the destination file system for the image file.</span>

## Create Image in the Destination Project
```
openstack image create NEW_IMAGE_NAME --container-format bare --disk-format qcow2 --file IMAGE_URL
```
## Boot the new Instance
```
openstack server create --flavor m1.tiny --image myInstanceSnapshot myNewInstance
```