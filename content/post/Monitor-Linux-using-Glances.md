---
title: Monitor your Linux box using Glances
date: 2019-04-27
tags: ["Linux", "System-Administration", "Monitoring"  ]
categories:  [ "Linux" ]
---
<!-- TOC -->

- [Preface](#preface)
- [Features](#features)
- [Installation](#installation)
- [Output](#output)

<!-- /TOC -->

## Preface
[Glances](https://nicolargo.github.io/glances/) is a cross-platform command-line curses-based system monitoring tool based on [Python](https://www.python.org/) Programming language. Glances is using  the psutil library to grab informations from the Operating Systm. Glance will monitr CPU, Load Average, Memory, Network Interfaces, Disk I/O, Processes and File System spaces utilization.

Glances is released under GPL, and works on FreeBSD and Linux systems. Many  options available in Glances.  Glance will allow the System administrator to set thresholds (Careful, Warning and Critical) in configuration file and informations will be shown in colors which indicates the bottleneck in the system.

## Features
* CPU Informations 
* Memory (Free, Alocated, Swap)
* Network Statistics
* Process Information
* Disk I/O 
* Mount information and Usage
* Top resource cosuming process
* Shows the current date and time at bottom.
* Highlights processes in Red that consumes highest system resources
  
## Installation
```
sudo apt install glances -y
```
## Output
![Glances ](/img/glances-usage-01.png)
