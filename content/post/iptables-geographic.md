---
title: Deny traffic based on Country Iptables
date: 2019-04-06
tags: ["Linux", "System-Administration", "Unix"  ]
categories:  [ "Linux" ]
---

## Preface
Some countries are blacklisted in ecommerce area due to spam flooding and DoS attacks. Its not difficult to find the root of an IP address. So the following script will block country based traffic in to your web server. The data will be updated in every day through a cronjob.
You need to visit this [SITE](http://www.ipdeny.com/ipblocks/) to download the zone file for which country you need to block.

* mkdir -p /opt/scripts

* vim /opt/scripts/ip_country_deny.sh

* Copy Paste the following *

```
#!/bin/bash
### Block all traffic from AFGHANISTAN (af) and CHINA (CN). Use ISO code ###
ISO="af cn"

### Set PATH ###
IPT=/sbin/iptables
WGET=/usr/bin/wget
EGREP=/bin/egrep

### No editing below ###
SPAMLIST="countrydrop"
ZONEROOT="/root/iptables"
DLROOT="http://www.ipdeny.com/ipblocks/data/countries"

cleanOldRules(){
$IPT -F
$IPT -X
$IPT -t nat -F
$IPT -t nat -X
$IPT -t mangle -F
$IPT -t mangle -X
$IPT -P INPUT ACCEPT
$IPT -P OUTPUT ACCEPT
$IPT -P FORWARD ACCEPT
}

# create a dir
[ ! -d $ZONEROOT ] && /bin/mkdir -p $ZONEROOT

# clean old rules
cleanOldRules

# create a new iptables list
$IPT -N $SPAMLIST

for c  in $ISO
do
# local zone file
tDB=$ZONEROOT/$c.zone

# get fresh zone file
$WGET -O $tDB $DLROOT/$c.zone

# country specific log message
SPAMDROPMSG="$c Country Drop"

# get
BADIPS=$(egrep -v "^#|^$" $tDB)
for ipblock in $BADIPS
do
   $IPT -A $SPAMLIST -s $ipblock -j LOG --log-prefix "$SPAMDROPMSG"
   $IPT -A $SPAMLIST -s $ipblock -j DROP
done
done

# Drop everything
$IPT -I INPUT -j $SPAMLIST
$IPT -I OUTPUT -j $SPAMLIST
$IPT -I FORWARD -j $SPAMLIST

# call your other iptable script
# /path/to/other/iptables.sh

exit 0
#Script Ends Here....
```
* Save and Exit

* Setup a crontab for working in every day

* vim /etc/crontab

*Add the following at the end of the file*

```
03 03 * * * root /opt/scripts/ip_country_deny.sh
```

*To start the script immediately*

```
/opt/scripts/ip_country_deny.sh
```

```
iptable -L -n -v
```

Check out the wonderful output....