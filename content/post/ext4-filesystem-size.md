---
title: EXT4 Filesystem Size
date: 2018-03-12
tags: ["Linux", "System-Administration", "Unix" ]
categories:  [ "Linux" ]
---

#### What is an EXT4

The ext4 file system is a scalable extension of the ext3 file system. It can support files and file systems up to 16 terabytes in size. It also supports an unlimited number of sub-directories (the ext3 file system only supports up to 32,000), though once the link count exceeds 65,000 it resets to 1 and is no longer increased. Ext4 uses extents (as opposed to the traditional block mapping scheme used by ext2 and ext3), which improves performance when using large files and reduces metadata overhead for large files. In addition, ext4 also labels unallocated block groups and inode table sections accordingly, which allows them to be skipped during a file system check. This makes for quicker file system checks, which becomes more beneficial as the file system grows in size.

#### Features

* Compatibility
* Bigger filesystem/file sizes
* Sub directory scalability
* Extents
* Multiblock allocation
* Delayed allocation
* Fast fsck
* Journal checksumming
* "No Journaling" mode
* Online defragmentation
* Inode-related features
* Persistent preallocation
* Barriers on by default

#### Formatting block volume to use as EXT4



