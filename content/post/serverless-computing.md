---
title: Debunking the Myth of Serverless Computing. The Importance of Baremetals
date: 2023-06-13
tags: ["Linux", "Virtualization", "Cloud"  ]
categories:  [ "Linux" ]
---
<!-- TOC -->

- [Introduction](#introduction)
- [Why We Need Physical Servers for Running a Serverless Environment](#why-we-need-physical-servers-for-running-a-serverless-environment)
    - [Foundation of Virtualization](#foundation-of-virtualization)
    - [Resource Allocation and Scaling](#resource-allocation-and-scaling)
    - [Hypervisor and Hardware Abstraction](#hypervisor-and-hardware-abstraction)
    - [Backend Operations and Management](#backend-operations-and-management)
    - [Security and Data Protection](#security-and-data-protection)
- [Conclusion](#conclusion)

<!-- /TOC -->


## Introduction

Serverless computing has gained significant popularity in recent years, promising developers the ability to focus solely on writing code without the need to manage server infrastructure. However, a closer examination reveals that serverless computing is not truly devoid of servers. In fact, every serverless environment relies on physical servers, as virtualization is essential for its functioning. This blog aims to shed light on the misconception surrounding serverless computing by emphasizing the crucial role of physical servers in such environments.

## Why We Need Physical Servers for Running a Serverless Environment

### Foundation of Virtualization
Serverless computing platforms are built on top of virtualization technology, which allows for the creation of virtual machines (VMs) and containers. However, virtualization is only possible with the presence of physical servers. These servers provide the necessary resources and infrastructure to host and manage the virtualized environments that power serverless platforms.

### Resource Allocation and Scaling
Physical servers play a pivotal role in resource allocation and scaling within a serverless environment. They provide the underlying computing power, memory, storage, and networking capabilities that are crucial for running applications and handling the dynamic scaling demands of serverless workloads. Without physical servers, it would be impossible to allocate and manage resources effectively, hindering the scalability benefits of serverless computing.

### Hypervisor and Hardware Abstraction
Serverless environments utilize hypervisors to abstract and manage the underlying hardware resources. Hypervisors, in conjunction with physical servers, enable the creation of multiple virtual machines or containers, allowing for efficient resource utilization. These hypervisors ensure isolation and security between different serverless functions or workloads, preventing interference or resource contention.

### Backend Operations and Management
Physical servers are vital for the proper functioning and management of the backend operations in a serverless environment. These servers handle tasks such as routing requests, load balancing, logging, monitoring, and orchestration. They are responsible for routing incoming requests to the appropriate serverless functions, coordinating their execution, and managing the overall performance and availability of the environment.

### Security and Data Protection
Physical servers form the foundation of security measures in a serverless environment. They provide the underlying infrastructure for implementing security controls, including firewalls, access controls, intrusion detection systems, and encryption mechanisms. Physical servers also enable the implementation of robust backup and disaster recovery strategies, ensuring the safety and integrity of data processed within the serverless environment.

## Conclusion

While the term "serverless" may suggest the absence of servers, it is essential to recognize that serverless computing relies heavily on physical servers. These servers form the backbone of the virtualized environments that power serverless platforms, enabling resource allocation, scaling, hardware abstraction, backend operations, security, and data protection. Understanding the role of physical servers in serverless computing allows us to appreciate the underlying infrastructure that makes this innovative paradigm possible, helping developers leverage its benefits more effectively.