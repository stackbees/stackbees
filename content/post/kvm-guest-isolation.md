---
title: KVM Guest creation &  isolation, How ?
date: 2020-07-03
tags: ["Linux", "Virtualization", "Unix", "Cloud"  ]
categories:  [ "Linux" ]
---

<!-- TOC -->

- [Preface](#preface)
- [Virtualization](#virtualization)
- [My Scenario](#my-scenario)
- [Process](#process)
- [Isolation](#isolation)
- [Further reading](#further-reading)

<!-- /TOC -->

## Preface
*`How can I make sure that,my virtual server is isolated from the neighbour in same compute host ?`* 

Almost all the cloud admins faced this question at least once in their career time. Customer is worrying about the privacy, security of their data. So, question is quiet natural. They completely trust you, but still question remains. How I am isolated from the neary by guests. I am trying to explain this case in simple words.

## Virtualization
Computing world is fastly moving to virtualization. Industry leading IT companies are hosting their workloads in Cloud. Virtualization gives you the ability to host multiple virtual servers on top of a single bare metal (physical) server. A cluster of multiple physical servers is called a Cloud. In a public cloud, there will be lot of customers, each customers have multiple virtual servers. Eventually same compute host will share its resources for multiple customers, multiple virtual machines. This is the first thing came into the mind of every IT decision makers while thinking about virtualization.

![Virtualization in Simple](/img/virtualization.png)

There will be a hardware (Baremetal), Host Operating system , Virtualization layer and Guest virtual machines. 

There is multiple virtualization softwares available in the market. Some are Free and Opensource, others are Copyright protected. Popular tools are as follows

| Tool   | Usage               | Company              |
|--------|---------------------|----------------------|
| KVM    | Free and Opensource | Community, Redhat    |
| Esxi   | Copyright applied   | VMWare               |
| HyperV | Copyright applied   | Microsoft            |
| Xen    | Free and Opensource | Community, Intel, LF |

In this article, i am explain about how guest isolation happening in `KVM`. The idea is same in all hypervisors. Currently, we dont have visibility on `Esxi` and `Hyperv` how it works in Kernel level

## My Scenario
* Physical server with Intel VT-x
* Ubuntu 16.04 as Host OS
* KVM as virtualization layer

KVM developers add this module in Linux Kernel. Once you enable this moduel, your host operating system will be acting as a hypervisor. It will virtulize your hardware resources. The main role of KVM is handling of the VM exits and execution of VM entry instructions. The KVM module itself cannot create a Virtual Machine. 
So, it will use a user space process called `QEMU`.  QEMU is inherently a hardware emulator. It was created even before KVM and can operate even without KVM.
QEMU is a software based emulator. 

## Process

* A file named /dev/kvm is created by the KVM kernel module
* This file enables  QEMU to convey a variety or requests  to the KVM kernel module
* When QEMU startups to execute a guest system, it makes continuous `ioctl()` system calls.

![Execution Flow](/img/qemu-execution-flow.png)

## Isolation

The QEMU creates a linux process for the newly created virtual machine and put it its own Name Space. There is one QEMU process for each Guest Virtual Machine. When multiple guest virtual machines are running, the same number of QEMU process are running. 

Namespaces are a feature of the Linux kernel that partitions kernel resources such that one set of processes sees one set of resources while another set of processes sees a different set of resources. The feature works by having the same namespace for a set of resources and processes, but those namespaces refer to distinct resources. Resources may exist in multiple spaces. Examples of such resources are process IDs, hostnames, user IDs, file names, and some names associated with network access, and interprocess communication.

The PID namespace provides processes with an independent set of process IDs (PIDs) from other namespaces. PID namespaces are nested, meaning when a new process is created it will have a PID for each namespace from its current namespace up to the initial PID namespace. Hence the parent process in a specific namespace can see all subprocesses.   

The first process created in a PID namespace is assigned the process id number 1 and receives most of the same special treatment as the normal init process, most notably that orphaned processes within the namespace are attached to it. This also means that the termination of this PID 1 process will immediately terminate all processes in its PID namespace and any descendants. 

## Further reading
* [KVM Technology](https://www.fujitsu.com/global/documents/about/resources/publications/fstj/archives/vol47-3/paper18.pdf)
* [KVM Address Space Isolation](https://lwn.net/Articles/788273/)
* [Linux Name space](https://en.wikipedia.org/wiki/Linux_namespaces)

