---
title: Creating  Virtualenvironment using Python 
date: 2019-10-22
tags: ["Linux", "Openstack", "Nova", "Programming"]
categories:  [ "Linux" ]
---


<!-- TOC -->

- [What is Python Virtualenvironment](#what-is-python-virtualenvironment)
- [Installation](#installation)
    - [Install pip](#install-pip)
    - [Install needed Python modules](#install-needed-python-modules)
    - [Set a Directory for your Environments](#set-a-directory-for-your-environments)
    - [Add it on your bashprofile](#add-it-on-your-bashprofile)
- [Testing the virtualenvironment](#testing-the-virtualenvironment)
- [Activate and Deactivate the environment](#activate-and-deactivate-the-environment)
- [Remove the Virtualenvironment](#remove-the-virtualenvironment)
- [Futher Reading](#futher-reading)

<!-- /TOC -->
## What is Python Virtualenvironment
Python applications will often use packages and modules that don’t come as part of the standard library. Applications will sometimes need a specific version of a library, because the application may require that a particular bug has been fixed or the application may be written using an obsolete version of the library’s interface.

This means it may not be possible for one Python installation to meet the requirements of every application. If application A needs version 1.0 of a particular module but application B needs version 2.0, then the requirements are in conflict and installing either version 1.0 or 2.0 will leave one application unable to run.

The solution for this problem is to create a virtual environment, a self-contained directory tree that contains a Python installation for a particular version of Python, plus a number of additional packages.

Different applications can then use different virtual environments. To resolve the earlier example of conflicting requirements, application A can have its own virtual environment with version 1.0 installed while application B has another virtual environment with version 2.0. If application B requires a library be upgraded to version 3.0, this will not affect application A’s environment.

## Installation
This installation was successfully done on `Ubuntu 18.04`. But it will work most of the Linux Distributions.

### Install pip
```
sudo apt install pip
```
### Install needed Python modules
```
pip install virtualenv or pip3 install virtualenv virtualenvwrapper
```
### Set a Directory for your Environments
```
export WORKON_HOME=~/Envs
```
### Add it on your bashprofile
```
export WORKON_HOME=~/Envs
source /home/bipindas/.local/bin/virtualenvwrapper.sh
```
## Testing the virtualenvironment
```
bipindas@bipindas-desk:~$ mkvirtualenv web
New python executable in /home/bipindas/Envs/web/bin/python
Installing setuptools, pip, wheel...
done.
virtualenvwrapper.user_scripts creating /home/bipindas/Envs/web/bin/predeactivate
virtualenvwrapper.user_scripts creating /home/bipindas/Envs/web/bin/postdeactivate
virtualenvwrapper.user_scripts creating /home/bipindas/Envs/web/bin/preactivate
virtualenvwrapper.user_scripts creating /home/bipindas/Envs/web/bin/postactivate
virtualenvwrapper.user_scripts creating /home/bipindas/Envs/web/bin/get_env_details
(web) bipindas@bipindas-desk:~$ 
```
You can see that, you changed to New environment. Now you can install your own versions

## Activate and Deactivate the environment
The following command will Activate your Environment
```
workon web
```
The following command will Deactivate your Environment
```
deactivate
```
## Remove the Virtualenvironment
```
rmvirtualenv web
Removing web...
```
## Futher Reading
* [Python Virtualenvironment](https://docs.python.org/3/tutorial/venv.html)
* [Python Virtualenvironment Introduction](https://www.geeksforgeeks.org/python-virtual-environment/)
