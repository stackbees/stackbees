---
title: Compute Service (Nova) Architecture 
date: 2019-04-13
tags: ["Openstack", "Opensource", "General" ]
categories:  [ "Opensource" ]
---


<!-- TOC -->

- [API server](#api-server)
- [Message queue](#message-queue)
- [Compute worker](#compute-worker)

<!-- /TOC -->


These basic categories describe the service architecture and information about the cloud controller.

## API server

At the heart of the cloud framework is an API server, which makes command and control of the hypervisor, storage, and networking programmatically available to users.

The API endpoints are basic HTTP web services which handle authentication, authorization, and basic command and control functions using various API interfaces under the Amazon, Rackspace, and related models. This enables API compatibility with multiple existing tool sets created for interaction with offerings from other vendors. This broad compatibility prevents vendor lock-in.

## Message queue

A messaging queue brokers the interaction between compute nodes (processing), the networking controllers (software which controls network infrastructure), API endpoints, the scheduler (determines which physical hardware to allocate to a virtual resource), and similar components. Communication to and from the cloud controller is handled by HTTP requests through multiple API endpoints.

A typical message passing event begins with the API server receiving a request from a user. The API server authenticates the user and ensures that they are permitted to issue the subject command. The availability of objects implicated in the request is evaluated and, if available, the request is routed to the queuing engine for the relevant workers. Workers continually listen to the queue based on their role, and occasionally their type host name. When an applicable work request arrives on the queue, the worker takes assignment of the task and begins executing it. Upon completion, a response is dispatched to the queue which is received by the API server and relayed to the originating user. Database entries are queried, added, or removed as necessary during the process.

## Compute worker

Compute workers manage computing instances on host machines. The API dispatches commands to compute workers to complete these tasks:

* Run instances
* Delete instances (Terminate instances)
* Reboot instances
* Attach volumes
* Detach volumes
* Get console output
* Network Controller

The Network Controller manages the networking resources on host machines. The API server dispatches commands through the message queue, which are subsequently processed by Network Controllers. Specific operations include:

* Allocating fixed IP addresses
* Configuring VLANs for projects
* Configuring networks for compute nodes