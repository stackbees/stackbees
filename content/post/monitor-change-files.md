---
title: Monitor file changes realtime
date: 2019-04-05
tags: ["Linux", "System Administration", "Devops"  ]
categories:  [ "Linux" ]
---

Everybody knows top or htop. Ever wished there was something similar but to monitor your files instead of CPU usage and processes? Well, there is.
Run this:

```
watch -d -n 2 ‘df; ls -FlAt;’
```

and you’ll get to spy on which files are getting written on your system. Every time a file gets modified it will get highlighted for a second or so. The above command is useful when you grant someone SSH access to your box and wish to know exactly what they’re modifying.

