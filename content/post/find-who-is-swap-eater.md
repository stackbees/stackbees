---
title: How to find the Swap usage of Guest VMs KVM
date: 2019-04-11
tags: ["Linux", "System-Administration", "Openstack"  ]
categories:  [ "Linux" ]
---
* How to find the Swap usage of Virtual Machines from a Swapped KVM host
```
find /proc -maxdepth 2 -path "/proc/[0-9]*/status" -readable -exec awk -v FS=":" '{process[$1]=$2;sub(/^[ \t]+/,"",process[$1]);} END {if(process["VmSwap"] && process["VmSwap"] != "0 kB") printf "%10s %-30s %20s\n",process["Pid"],process["Name"],process["VmSwap"]}' '{}' \;|grep qemu
```

* Output

```
     3529 qemu-system-x86                            15112 kB
      6362 qemu-system-x86                           151732 kB
     16441 qemu-system-x86                            32040 kB
     25338 qemu-system-x86                           267232 kB
     29052 qemu-system-x86                            66408 kB
     34747 qemu-system-x86                             7252 kB
     34876 qemu-system-x86                            26576 kB
     44797 qemu-system-x86                            11164 kB
     44838 qemu-system-x86                            77052 kB
     45507 qemu-system-x86                            13928 kB
     53475 qemu-system-x86                             8144 kB
     55232 qemu-system-x86                            22288 kB
```