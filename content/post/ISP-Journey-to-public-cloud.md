---
title: An ISP Journey to Public Cloud 
date: 2018-03-07
tags: ["Openstack", "Opensource", "General" ]
categories:  [ "Opensource" ]
---

<!-- TOC -->

- [Transformation from Baremetal to Virtualization](#transformation-from-baremetal-to-virtualization)
- [A change is inevitable](#a-change-is-inevitable)
- [OK, What will be our prototype](#ok-what-will-be-our-prototype)
- [Here is the Leader](#here-is-the-leader)
- [Openstack](#openstack)
- [Mirantis](#mirantis)
- [Release](#release)
- [Design](#design)
- [Rainfall](#rainfall)
- [What about Cost](#what-about-cost)
- [Aftermath](#aftermath)

<!-- /TOC -->

### Transformation from Baremetal to Virtualization

In 2012, when we think about Virtualization, people are not ready to accept the changes. They were not believing in Virtualization because of following reasons.

* Virtual ?
* Security ?
* Data Integrity ?
* Availability ?
* Management ?

Even it will be a cost effective from `CIO / CTO` point of view, they were afraid of the above. And of course, they were believing that Baremetal is effective.
Changing this mindset was a hard job. For this Market, money is not a problem. Decision making even not. So, Virtualization story is crawling like Snail, or even slow :-).
A long time before, specifically 6 Year before, amazon lauched their Cloud Platform called, [Amazon Web Services](https://aws.amazon.com/) for public use. 
Still, we were happy with Baremetal.

### A change is inevitable

In the fall of 2012, We were able to convince the Management about the need of Cloud in the Market. Convinced, half minded. But gave a Green signal to build a protoype to understand what is this. Same time, one of leading ISP in the country was building a public cloud based on VMWare. In a short period, this goes live and response from the market was amazing. Within 3 months, they grab a huge business which. The plate became really hot . 

### OK, What will be our prototype

We didnt thought about an option other than KVM. We had made a vendor matrix and choose [Abiquo](https://www.abiquo.com/) as our vendor. There was many at the market like OnAPP, but Abiquo was only provider who supports multi hypervisor. Multihypervisor was a vision at that time, but never take off. We build a  prototype with 3 hypervisors, I said three (3) and we went live in 3 months. There was not Marketing / Sales support for this project. But using our brand name, we got some good accounts. Customers were asking for more resources, more enquiries hitting the sales inbox. People start believing in the product. Even commercial team asked for more resources to onboard new customers, unfortunately scaling out was a disaster for the particular setup.

### Here is the Leader

There was a new team created end of 2014 only for building a New cloud in the region. And a new Leader changes everything, even the way we works. After that transformation was Quick. Every one start working like an oiled machine. Research & Development, Reading, Discussion with Vendors. A revolution starts there.

### Openstack

We reached in Openstack. Juno was the release at that time. We had downloaded the upstream and start installing it in 9 very old servers. Even HP company may not remember about those models. Such an old models. Yeah !! Openstack doesnt care about Server Models. At the end he is seeing the CPU / Disk and Memory. Yes, the Lab setup was ready. This is what we made.

| 3 Controllers | 3 Computes | 3 Ceph Storage |
|---------------|------------|----------------|

We made it and it was just a working model only. For the time being, our CISCO UCS servers are on the way to the Data Center.  The following are the heavy metals


| Processor                         | Chipset           | Memory | Storage | NIC         |
|-----------------------------------|-------------------|--------|---------|-------------|
| Intel® Xeon® processor E5-2600 v3 | Intel C610 series | 755 GB | 500GB   | 10-Gbps x 2 |

It was considered to be a Beta  environment targeting to our premium customers. Totall 50 Servers. We were planned to use NetAPP FAS 8040 as storage. Due to limitations of creating volume in this NetAPP series, we forced to use NFS instead of iSCSI. Idea was not completely convinced by team members, but no other options to choose. Ceph was an option, but commercial support is too expensive as long run at that time.

### Mirantis

The pure play openstack company. The next thing is we need a technology consultant who have experience in Openstack. We had made deep discussions with [Redhat](https://www.redhat.com/en/technologies/linux-platforms/openstack-platform), [Canonical](https://www.ubuntu.com/openstack), [Mirantis](https://www.mirantis.com/software/openstack/). All three were the leaders in the Market at that time. We choose Mirantis for the following reasons

* Openstack is their only business
* Leading contributor in the Openstack Upstream kernel. [Stackalytics](http://stackalytics.com/?release=juno&project_type=all)
* Build Openstack cloud for Webex, Paypal at that time
* Ericsson invest a huge amount in Mirantis for their NFV project
* Distro is Free & Opensource

### Release

2015, Summer we start building our Beta cloud using MOS 6.1. The Distro was built using [Openstack Juno](https://www.openstack.org/software/juno/), 10th Openstack Release. It was long journey, But happy news was Management provided  a breathing time for deploying. 

### Design

It is a 65 Server farm. 

![High Level Design](/img/hld-beta.png)

Baremetals are deployed in the following way.

| Control Plane | Data Plane | Swift Proxy | Swift Nodes |
|---------------|------------|-------------|-------------|
| 3             | 45         | 3           | 10          |


### Rainfall

We were very excited while watching the deployment process ends with an OK status. We spawned our first virtual machine based on Cirros image. Connected with Floating IP and it reaches Internet. Viola !!!, we call this project as ``` Rainfall ``` . October 2015 we launched the service for customers as production. We planned for a Beta release, but after number of Benchmarks and Validation tests, we ourself confident for a Production Release.

![Success](/img/openstack-ready.png)

### What about Cost

CAPEX & OPEX were 25 percent below to those proprietary alternatives and likely to exceed that benefit soon. OpenStack, there’s always room for cost improvement because the community is evolving and delivering better performance and management tools. 

### Aftermath

It became a success. We overtake our next competetor in 4 months, Thats tells the whole story. Journey continues. We have multiple Datacenters in the Region. Serving hundreds of customers, thousands of Virtual machines, Terabytes of memory and Many custom build applications for ready-to-use.

