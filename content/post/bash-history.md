---
title: Bash Tips
date: 2019-05-02
tags: ["Linux", "System-Administration", "Tips&Tricks"  ]
categories:  [ "Linux" ]
---
<!-- TOC -->

- [Preface](#preface)
- [History](#history)
- [Using `!`](#using-)
- [Using part of Command](#using-part-of-command)

<!-- /TOC -->

# Preface
One of the fancy feature of Bash is the command history. History will store all the command run by the User, which will be in /home/$USER/.bash_history. This will help the user to edit / reclass previously applied commands. To get a command previous run in command you can use `UP` arrow or `DOWN` arrow keys. 

# History
```

  288  cd layouts/
  289  ls
  290  cd ..
  291  ls
  292  cd ..
  293  ls
  294  tar -zcvf views.tar.gz views/
  295  ls
  296  cd public/
  297  ls
  298  cd ..
  299  ls
  300  scp views.tar.gz ubuntu@192.168.1.2:/home/ubuntu/
  301  cd ..
  302  ls
  303  tar -zcvf openKB.tar.gz openKB/
  304  ls
  305  rm -rf openKB.tar.gz 
  306  pm2 list
```
# Using `!` 

For example if you want to run the `306` line, enter this command

```
!306
```
Output
```
ubuntu@personal-knowledgebase:~$ !306
pm2 list
┌──────────┬────┬──────┬──────┬────────┬─────────┬────────┬─────┬────────────┬────────┬──────────┐
│ App name │ id │ mode │ pid  │ status │ restart │ uptime │ cpu │ mem        │ user   │ watching │
├──────────┼────┼──────┼──────┼────────┼─────────┼────────┼─────┼────────────┼────────┼──────────┤
│ app      │ 0  │ fork │ 4457 │ online │ 0       │ 37D    │ 0%  │ 992.2 MB   │ ubuntu │ disabled │
└──────────┴────┴──────┴──────┴────────┴─────────┴────────┴─────┴────────────┴────────┴──────────┘
 Use `pm2 show <id|name>` to get more details about an app
ubuntu@bluvalt-knowledgebase:~$ 
```
# Using part of Command
Also, you can use the partial of the Command as well. 

```
!pm2
```
Output will be Same
```
ubuntu@personal-knowledgebase:~$ !pm2
pm2 list
┌──────────┬────┬──────┬──────┬────────┬─────────┬────────┬─────┬────────────┬────────┬──────────┐
│ App name │ id │ mode │ pid  │ status │ restart │ uptime │ cpu │ mem        │ user   │ watching │
├──────────┼────┼──────┼──────┼────────┼─────────┼────────┼─────┼────────────┼────────┼──────────┤
│ app      │ 0  │ fork │ 4457 │ online │ 0       │ 37D    │ 0%  │ 995.2 MB   │ ubuntu │ disabled │
└──────────┴────┴──────┴──────┴────────┴─────────┴────────┴─────┴────────────┴────────┴──────────┘
 Use `pm2 show <id|name>` to get more details about an app
ubuntu@personal-knowledgebase:~$ 
```


