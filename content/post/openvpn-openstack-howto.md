---
title: How to install OpenVPN in Openstack Environment
date: 2019-04-10
tags: ["Linux", "System-Administration", "Network", "Openstack"  ]
categories:  [ "Linux" ]
---
<!-- TOC -->

- [Preface](#preface)
- [Use Case](#use-case)
- [Deployment](#deployment)
- [Setup](#setup)
- [Installation](#installation)
- [VDC Configuration](#vdc-configuration)
- [Client Configuration](#client-configuration)

<!-- /TOC -->

## Preface
OpenVPN is an open-source commercial software that implements [Virtual Private Network](https://en.wikipedia.org/wiki/Virtual_private_network) (VPN) techniques to create secure point-to-point or site-to-site connections in routed or bridged configurations and remote access facilities. It uses a custom security protocol that utilizes [SSL/TLS](https://en.wikipedia.org/wiki/Transport_Layer_Security) for key exchange. It is capable of traversing network address translators (NATs) and firewalls. It was written by James Yonan and is published under the GNU [General Public License](https://en.wikipedia.org/wiki/GNU_General_Public_License).

## Use Case
Some times you need to connect to your Virtual Datacenter over a Secured network. The commercial applications like, [Palo Alto](https://en.wikipedia.org/wiki/Palo_Alto_Networks) are very expensive. [OpenVPN](https://en.wikipedia.org/wiki/OpenVPN) is an opensource implementation of IP/VPN server. You can install OpenVPN in your VDC and bring all your virtual machines under this Server. Later you will be connected to your VDC using this server.

## Deployment

A high level diagram of the Deployment. 

![High Level Design](/img/openvpn-deployment.png)

## Setup 
We have Three networks.

* DB Network - 192.168.1.0/24
* Web Farm - 172.18.10.0/24
* VPN Network Out - 10.10.10.0/24

Virtual Machines under First two networks doesnt have a Floating IP Address. At the end, VMs residing on these network cannot be accessible from Internet directly.  The third network, `VPN Network Out` , in this network we created Our VPN Server. And we will assign a Floating IP Address to this Server.

## Installation

* Create a [Virtual Server](https://kb.bluvalt.com/uploads/create_VM.mp4). Preferrably Ubuntu 16.04
* Download the Installation Script from [Here](https://raw.githubusercontent.com/Nyr/openvpn-install/c6880407ddb0fa10ab089804df1723d7d1a71463/openvpn-install.sh)

```
sudo bash openvpn-install.sh
```
The following screen will be Displayed.
![Installation Precheck](/img/installation-precheck.png)

* Script will Automatically Detect the Internal IP Address
* By default `1194` UDP port will be used. You change this if you need.
* The preferred DNS, I choose Google DNS, I trust [Google](https://google.com) very well :)
* Finally, Enter a name for your Client Configuration file.
* Hit Enter, and sit back

Script will automatically Download a lot of files and creating Certificates
![Installation on the Fly](/img/installation-fly.png)

Finally it will ask for your Floating IP Address, enter it and Finished.

## VDC Configuration
* Create a Router `VPN-GW-Router`
* Add Interface, Choose all the Three Networks
* Create a Security Group `VPN-SG-Out`
* Add the below rules

```
Ingress	IPv4	UDP	1194	        0.0.0.0/0
Ingress	IPv4	TCP	22 (SSH)	0.0.0.0/0
```
<div style="background-color: #cfc ; padding: 10px; border: 1px solid green;" <span>Attach above Rule to VPN Server</span> </div>


* Create a Security Group `VPN-Access`
* Add the below rules
  
```
Ingress	IPv4	ANY	0 - 65535	10.8.0.0/24
Ingress	IPv4	ANY	0 - 65535	172.18.80.3/32
```

<div style="background-color: #cfc ; padding: 10px; border: 1px solid green;" <span>Attach above Rule to All the VMs you Created</span> </div>

## Client Configuration
* Download the Appropriate client from [here](https://openvpn.net/community-downloads/)
* Install it
* Open the Application
* Import the File, Choose your Client Configuration
* It will Open a Window, saying that You are Connected !!

<div style="background-color: #cfc ; padding: 10px; border: 1px solid green;" <span>Make sure you have added a Security Group rule for 1194 UDP for VPN Server</span> </div>

****

<div style="background-color: #cfc ; padding: 10px; border: 1px solid green;" <span>Make sure, 3 Interfaces added for your Router. This is for three Networks</span> </div>



