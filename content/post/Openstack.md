---
title: Openstack 
date: 2018-03-07
tags: ["General", "Opensource" ]
categories:  [ "Opensource" ]
---

## What is Openstack

OpenStack is a free and open-source software platform for cloud computing, mostly deployed as infrastructure-as-a-service (IaaS), whereby virtual servers and other resources are made available to customers. The software platform consists of interrelated components that control diverse, multi-vendor hardware pools of processing, storage, and networking resources throughout a data center. Users either manage it through a web-based dashboard, through command-line tools, or through RESTful web services.

## Evolution

In July 2010, Rackspace Hosting and NASA jointly launched an open-source cloud-software initiative known as OpenStack. The OpenStack project intended to help organizations offer cloud-computing services running on standard hardware. The community's first official release, code-named Austin, appeared three months later on 21 October 2010, with plans to release regular updates ofthe software every few months. The early code came from NASA's Nebula platform as well as from Rackspace's Cloud Files platform. The original cloud architecture was designed by the NASA AmesWeb Manager, Megan A. Eskey, and was a 2009 open source architecture called OpenNASA v2.0. The cloud stack and open stack modules were merged and released as open source by the NASA Nebula team in concert with Rackspace.

## Features

* Free and Opensource
* Based On Linux
* Leverages commodity hardware
* Multi-dimensional scalability
* Easy Scale
* Community and Professional support

## Releases

| NO | Release | Date              | EOL        |
|----|---------|-------------------|------------|
| 1  | Rockey  | 2018-08-30        | 2020-02-24 |
| 2  | Queens  | 2018-02-28        | 2019-02-25 |
| 3  | Pike    | 2017-08-30        | 2018-09-03 |
| 4  | Ocata   | 2017-02-22        | 2018-08-27 |

* Last 4 Releases are included, For the Complete list Click [Here](https://releases.openstack.org/).
