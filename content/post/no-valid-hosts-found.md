---
title: No valid hosts found !! , Is it Hypervisor only?
date: 2019-11-14
tags: ["Openstack", "nova", "cloud-operation" ]
categories:  [ "Linux" ]
---
<!-- TOC -->

- [No valid hosts found](#no-valid-hosts-found)
- [Real scenario](#real-scenario)
- [Oh, yeah here it is](#oh-yeah-here-it-is)
- [Issue](#issue)
- [Finding](#finding)
- [Further reading](#further-reading)

<!-- /TOC -->

## No valid hosts found
I am sure, everyone of you found this error in your API call OR Horizon instance intiation. But what is it exactly?, Well the error says `no valid hosts found` which is clear right? You dont have enough computing resource to launch the new virtual machine. It could be `vCPU`, `vRAM`, `vDISK`. When you check the `hypervisor status` you realize that, you have enough resources. Then what is this error?

## Real scenario
One of our Customer reported that, he was getting the following error while creating an instance. We are running a public cloud and we know about our resources. 
![Error ](/img/no-valid-hosts-found.png)
Our Service center agents got panic and reported a Severity 1 case with L3 team saying that, `Oh, we have a resource issue`. We immediately engaged the issue. Unfortunately, the customer immediately deleted the virtual machine once he faced this error. We tried to reproduce on our tenants, nothing found, all Good. We asked the customer to try again. 

## Oh, yeah here it is

Customer initiated the VM creation, all looks good untill `nova` asked `neutron` to create a port for this incoming Guest. Nova reported the below.

```
2019-11-13 17:41:58.427 44267 WARNING nova.network.neutronv2.api  Neutron error: No more fixed IPs in network: be05d158-c030-42e1-a421-64fd724d950a
2019-11-13 17:41:59.072 44267 ERROR nova.compute.manager [-]  Instance failed to spawn
2019-11-13 17:41:59.072 44267 TRACE nova.compute.manager  NoMoreFixedIps: Zero fixed ips available.
```
## Issue
Customer choose a network `/27` for his virtual machine. In a `/27` network, only 30 IPaddress available. He already consumed `28`, One reserved for `Broadcast` and one reserved for `Gateway`. So, he is out of network resource. We advise him to choose any other network with enough IP available. He choose another network and VM spawned successfully.

## Finding
`no valid hosts found` is a general message only. It could be anything. `Logs` are your friend :-). 

## Further reading
* [Article on Redhat portal](https://access.redhat.com/documentation/en-us/red_hat_openstack_platform/8/html/director_installation_and_usage/sect-troubleshooting_no_valid_host_found_errors)
* [Article on IBM portal](https://www.ibm.com/support/knowledgecenter/SS4KMC_2.5.0.4/com.ibm.ico.doc_2.5/ts/r_failed_deploying.html)