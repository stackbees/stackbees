---
title: Benchmarking SSD
date: 2019-04-05
tags: ["Linux", "System-Administration", "Unix" , "Storage" ]
categories:  [ "Linux" ]
---
<!-- TOC -->

- [How to Test Write Speed](#how-to-test-write-speed)
- [How to Test Read Speed](#how-to-test-read-speed)

<!-- /TOC -->
## How to Test Write Speed

```
sync; dd if=/dev/zero of=tempfile bs=1M count=1024; sync
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB) copied, 3.28696 s, 327 MB/s
```

## How to Test Read Speed

```
dd if=tempfile of=/dev/null bs=1M count=1024
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB) copied, 0.159273 s, 6.7 GB/s
```

* Clear the Cache and Run again

```
 sudo /sbin/sysctl -w vm.drop_caches=3
vm.drop_caches = 3
$ dd if=tempfile of=/dev/null bs=1M count=1024
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB) copied, 2.27431 s, 472 MB/s
```
<div style="background-color: #cfc ; padding: 10px; border: 1px solid blue;" <span>Figures may varies, based on Disk. Check with your Vendor</span> </div>