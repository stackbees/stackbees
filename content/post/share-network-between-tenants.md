---
title: Share Private network between Two Tenants
date: 2019-07-30
tags: ["Linux", "System-Administration", "Openstack", "Network"  ]
categories:  [ "Linux" ]
---
<!-- TOC -->

- [Courtesy](#courtesy)
- [Preface](#preface)
- [Assumptions](#assumptions)
- [Solution](#solution)
- [Import Route Target](#import-route-target)
- [Security Group](#security-group)
- [Futher Reading](#futher-reading)

<!-- /TOC -->


## Courtesy

`Bytes` around me :-)

## Preface

Sharing Private network is a common use case in Cloud Environment. Normally [RBAC](https://docs.openstack.org/newton/networking-guide/config-rbac.html) will help you to achieve. Since [Newton](https://www.openstack.org/software/newton/) release, this feature is Available. But the question is which SDN you are using in your Environment. If you are using Opencontrail 3.2 as SDN, you cannot go with RBAC. 

## Assumptions

* [Openstack Release Ocata](https://www.openstack.org/software/ocata/)
* [Opencontrail 3.2](https://www.juniper.net/documentation/en_US/contrail3.2/information-products/topic-collections/release-notes/index.html)
* We have Network called `Project-Network-01` in `Project-01`
* We have Network Called `Project-Network-02` in `Project-02`
* Both VDC are in Same Datacenter

## Solution

Each network we created in Contrail, will have a route target. So, the Idea in High Level is to Import Route target between each Network. 

while you are going to Contrail, you can find Two route targets for the project `Project-01` and the nework `Project-Network-01`, 

* Login to Contrail GUI
* Click on the `Gear` Icon, left Top
* Click `Introspect`

In Right pane, 

* Chose IP Address (Chose any one of the Controller IP)
* Chose `bgb_peer` from `Module`  
* Chose `ShowRoutingInstanceReq` from  `Request` 
* In the `SearchString` Enter Project name and Network Name `Project-01:Project-Network-01`

![Check RT](/img/interospect.png)

You can see multiple route targets under `export_target` tab. For example, you will see the below

```
target:64814:8002398
```

Copy the Last 4 Digits of the route target and Open a Duplicate copy of your Session in another Tab

* Chose IP Address (Chose any one of the Controller IP)
* Chose `bgb_peer` from `Module`  
* Chose `ShowRtGroupReq` from `Request` 
* In the `SearchString` Enter Digits we copied `8002398`

Go to `XSL Grid` tab and search for `snat` , if you are not find anything related to `snat` you are good to Go.

## Import Route Target

* Go to the Home page of Contrail GUI
* Click on the `Spanner` Icon, left Top
* Click `Networking`

![Network Selection](/img/network-selection.png)

Now all your Projects in the Environment will be Listed. Chose the `Project-02` which is our Destination project to import the RT

* `Check` the Network you want to Share with `Project-01` and Click on the `Gear` Icon on right Side and select `Edit`
* Click `Import Route Target(s)`
* Click the `+` sign to Add another row
* In the `ASN` part enter this `64814` and in `Target` Part Enter this `8002398` 

![Import RT](/img/import-route-target.png)

## Security Group

* Create a New Security Group in `Project-02`
* Allow the `Project-Network-01` Subnet in `Project-02`

## Futher Reading

* [Sharing Private Subnet in Amazon](https://docs.aws.amazon.com/vpc/latest/userguide/example-vpc-share.html)
* [RBAC In Openstack Networking](https://docs.openstack.org/newton/networking-guide/config-rbac.html)
* [Openstack Networking Ocata](https://docs.openstack.org/ocata/networking-guide/)


