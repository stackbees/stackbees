---
title: Benchmarking Cloud using Rally
date: 2019-05-05
tags: ["Linux", "Openstack", "Rally", "Cloud"  ]
categories:  [ "Linux" ]
---
<!-- TOC -->

- [Preface](#preface)
- [Installation](#installation)
- [Create a Test Scenario](#create-a-test-scenario)
- [Export the Test Results to HTML](#export-the-test-results-to-html)
- [More Scenarios](#more-scenarios)

<!-- /TOC -->


## Preface
[Rally](https://github.com/openstack/rally) is a framework used to Validate multinode [Openstack](https://www.openstack.org/) Deployment. Rally automates and unifies multi-node OpenStack deployment, cloud verification, benchmarking & profiling. Rally does it in a pluggable way, making it possible to check whether OpenStack is going to work well on, say, a 1k-servers installation under high load. Thus it can be used as a basic tool for an OpenStack CI/CD system that would continuously improve its SLA, performance and stability.

## Installation 

Rally is a lightweight framework which can be installed on a docker container. What we need is, the docker container can access the Openstack API endpoints.
```
apt-get update
apt-get install docker.io
usermod -a -G docker `id -u -n`  #add yourself to docker group. its important to exit and re-login
mkdir ~/rally_home
chown 65500 ~/rally_home
```
```
docker pull rallyforge/rally
```
```
docker run -t -i -v ~/rally_home:/home/rally rallyforge/rally
```
*inside the container and save it as myenv.json (All the below command will be executed inside the container including this)*
```
{
"type": "ExistingCloud",
"auth_url": "http://172.19.212.2:5000/v2.0/",
"region_name": "RegionOne",
"endpoint_type": "public”,
"https_insecure": true,
"admin": {
 "username": "admin",
      "password": "admin",
        "tenant_name": “admin”
}
}
```
```
rally-manage db recreate
rally deployment create --filename myenv.json —name myenv
```
Output
```
+--------------------------------------+----------------------------+-------+------------------+--------+
| uuid                                 | created_at                 | name  | status           | active |
+--------------------------------------+----------------------------+-------+------------------+--------+
| 519c4a5a-6e76-470f-8c5d-095ac63a4a4b | 2019-05-05 08:12:21.286459 | myenv | deploy->finished |        |
+--------------------------------------+----------------------------+-------+------------------+--------+
```

rally deployment list

```
+--------------------------------------+----------------------------+-------+------------------+--------+
| uuid                                 | created_at                 | name  | status           | active |
+--------------------------------------+----------------------------+-------+------------------+--------+
| 519c4a5a-6e76-470f-8c5d-095ac63a4a4b | 2019-05-05 08:12:21.286459 | myenv | deploy->finished | *      |
+--------------------------------------+----------------------------+-------+------------------+--------+
```
Check the environment
```
rally deployment check
```
```
keystone endpoints are valid and following services are available:
+-------------+---------------------+-----------+
| services    | type                | status    |
+-------------+---------------------+-----------+
| cinder      | volume              | Available |
| cloud       | cloudformation      | Available |
| ec2         | ec2                 | Available |
| glance      | image               | Available |
| heat        | orchestration       | Available |
| keystone    | identity            | Available |
| neutron     | network             | Available |
| nova        | compute             | Available |
| s3          | s3                  | Available |
| swift       | object-store        | Available |
+-------------+---------------------+-----------+
```
## Create a Test Scenario
```
{
    "CinderVolumes.create_and_list_volume": [
        {
            "args": {
                "size": 1,
                "detailed": true
            },
            "runner": {
                "type": "constant",
                "times": 3,
                "concurrency": 1
            },
            "context": {
                "users": {
                    "tenants": 1,
                    "users_per_tenant": 1
                }
            }
        },
        {
            "args": {
                "size": {
                    "min": 1,
                    "max": 5
                },
                "detailed": true
            },
            "runner": {
                "type": "constant",
                "times": 3,
                "concurrency": 1
            },
            "context": {
                "users": {
                    "tenants": 1,
                    "users_per_tenant": 1
                }
            }
        }
    ]
}
```
*save the above like this cinder_create_and_delete_volume.json. Creating and listing volumes.*
```
rally task start cinder_create_and_delete_volume.json
```
## Export the Test Results to HTML
```
rally task report 242bad1e-91b0-4278-8046-9905ff50999e --out output.html
```
![HTML Output ](/img/create-and-list-volumes.png)
## More Scenarios
The comprehensive list of test scenarios is available on Rally [GitHub repo](https://github.com/openstack/rally/tree/master/samples/tasks/scenarios) . You can download from the repo and change the values according to your settings.





