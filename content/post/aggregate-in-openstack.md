---
title: Aggregate in Openstack
date: 2019-04-05
tags: ["Linux", "Openstack", "Nova"  ]
categories:  [ "Linux" ]
---
<!-- TOC -->

- [Preface](#preface)
- [List the available hypervisor list to add under this zone.](#list-the-available-hypervisor-list-to-add-under-this-zone)
- [Now, we are going to add hypervisor "compute1" under Ezone-IND](#now-we-are-going-to-add-hypervisor-%22compute1%22-under-ezone-ind)

<!-- /TOC -->
## Preface
People may have little bit of confusion about Host Aggregates and Availabaility Zones in Nova. Here I am trying to explain how these things are worked together. All configuration options aer based on Icehouse release of Openstack.

A host aggregate is a grouping of hosts with associated metadata. A host can be in more than one host aggregate.  The concept of host aggregates is only exposed to cloud administrators. A host aggregate may be visible to users in the form of an availability zone. When you create a host aggregate, you have the option of providing an availability zone name.

The following will create a Availability zone called Ezone-IND with a Name of enterprise.

```
[root@controller ~]# nova aggregate-create enterprise Ezone-IND
+----+------------+-------------------+-------+-------------------------------+
| Id | Name       | Availability Zone | Hosts | Metadata                      |
+----+------------+-------------------+-------+-------------------------------+
| 3  | enterprise | Ezone-IND         |       | 'availability_zone=Ezone-IND' |
+----+------------+-------------------+-------+-------------------------------+
```

## List the available hypervisor list to add under this zone.
```
root@controller ~]# nova hypervisor-list
+----+----------------------+
| ID | Hypervisor hostname  |
+----+----------------------+
| 1  | compute              |
| 4  | domain-c15(Cluster1) |
| 5  | compute1             |
+----+----------------------+
```
## Now, we are going to add hypervisor "compute1" under Ezone-IND
```
[root@controller ~]# nova aggregate-add-host 3 compute1
Host compute1 has been successfully added for aggregate 3 
+----+------------+-------------------+------------+-------------------------------+
| Id | Name       | Availability Zone | Hosts      | Metadata                      |
+----+------------+-------------------+------------+-------------------------------+
| 3  | enterprise | Ezone-IND         | 'compute1' | 'availability_zone=Ezone-IND' |
+----+------------+-------------------+------------+-------------------------------+
```
Now you can see the Host Compute1 is under Ezone-IND.
