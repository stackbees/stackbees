---
title: Change network bandwidth of KVM Guest OS
date: 2019-07-18
tags: ["Linux", "Cloud", "Tips&Tricks", "KVM"  ]
categories:  [ "Linux" ]
---

## Preface
Some cloud providers, limited the Bandwidth based on Flavors. Which is part of their Business rules. So, Guest virtual machines wont be able to reach the limit which fixed in the Flavor using metadata. This tutorial will help you to change the Network BW without restarting the Guest virtual machine.

# List the Current

```
virsh domiflist instance-0000be23
Interface  Type       Source     Model       MAC
-------------------------------------------------------
tap973d62dc-4f bridge     qbr973d62dc-4f virtio      fa:16:3e:fb:24:e0

```
```
virsh domiftune instance-0000be23 tap973d62dc-4f
inbound.average: 12800
inbound.peak   : 12800
inbound.burst  : 0
outbound.average: 12800
outbound.peak  : 12800
outbound.burst : 0
```
As you can see the current limit is 100Mbit/s (12800). 

## Change to 4G

```
virsh domiftune instance-0000be23 tap973d62dc-4f --config --live --inbound 524288,524288,0 --outbound 524288,524288,0
```
```
virsh domiftune instance-0000ab85 tap973d62dc-4f
inbound.average: 524288
inbound.peak   : 524288
inbound.burst  : 0
outbound.average: 524288
outbound.peak  : 524288
outbound.burst : 0
```