---
title: Diagnose network with MTR
date: 2019-04-07
tags: ["Linux", "System-Administration", "Network"  ]
categories:  [ "Linux" ]
---

## Preface
MTR is a powerful tool which enables administrators to diagnose and isolate networking errors and provide reports of network status to upstream providers. MTR represents an evolution of the traceroute command by providing a greater data sample, as if augmenting `traceroute` with `ping` output. This document provides an in depth overview of MTR, the data it generates, and how to interpret and draw conclusions based on the data provided by it.

## Install MTR in Ubuntu
```
apt update && apt upgrade
apt install mtr-tiny
```
```
stackbees@stackbees-desk:~$ mtr -rw google.com
Start: 2019-04-07T10:35:55+0300
HOST: stackbees-desk   Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- _gateway         0.0%    10    0.4   3.7   0.3  34.0  10.6
  2.|-- 10.70.78.25      0.0%    10    0.2   0.3   0.2   0.5   0.1
  3.|-- 10.70.80.1       0.0%    10    0.7   0.7   0.6   0.9   0.1
  4.|-- 213.184.165.193  0.0%    10    2.3   6.3   1.2  39.1  11.7
  5.|-- 212.93.211.137   0.0%    10    0.9   1.1   0.8   3.8   0.9
  6.|-- ???             100.0    10    0.0   0.0   0.0   0.0   0.0
stackbees@stackbees-desk:~$ 
```
The report was generated with mtr --report `google.com`. This uses the report option, which sends 10 packets to the host `google.com` and generates a report. Without the `--report` option, mtr will run continuously in an interactive environment. The interactive mode reflects current round trip times to each host. In most cases, the `--report` mode provides sufficient data in a useful format.

Each numbered line in the report represents a hop. Hops are the Internet nodes that packets pass through to get to their destination. The names for the hosts (e.g. “inner-cake” and “outer-cake” in the example) are determined by reverse DNS lookups. If you want to omit the rDNS lookups you can use the --no-dns option, which produces output similar to the following:

```
mtr --no-dns --report google.com
Start: 2019-04-07T10:39:28+0300
HOST: stackbees-desk               Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 10.92.4.1                  0.0%    10    0.5   0.4   0.3   0.6   0.1
  2.|-- 10.70.78.25                0.0%    10    0.3   0.3   0.2   0.3   0.0
  3.|-- 10.70.80.1                 0.0%    10    0.6   0.6   0.6   0.7   0.0
  4.|-- 213.184.165.193            0.0%    10    1.6   3.1   1.2   5.9   2.0
  5.|-- 212.93.211.137             0.0%    10    0.9   0.8   0.8   0.9   0.0
  6.|-- ???                       100.0    10    0.0   0.0   0.0   0.0   0.0
```

## Analyse the Report
When analyzing MTR output, you are looking for two things: loss and latency. If you see a percentage of loss at any particular hop, that may be an indication that there is a problem with that particular router. However, it is common practice among some service providers to rate limit the ICMP traffic that MTR uses. This can give the illusion of packet loss when there is in fact no loss. 

