---
title: CURL, find your website Speed.
date: 2019-05-02
tags: ["Linux", "System-Administration", "Tips&Tricks"  ]
categories:  [ "Linux" ]
---
Page loading speed is very important now a days.  If you are a web administrator, or  a server administrator who is particularly responsible for organizing the things together, then you have to make it a point that users don’t get disappointed while accessing your site – so there is really `need for speed`

```
curl -s -w 'Testing Website Response Time for :%{url_effective}\n\nLookup Time:\t\t%{time_namelookup}\nConnect Time:\t\t%{time_connect}\nPre-transfer Time:\t%{time_pretransfer}\nStart-transfer Time:\t%{time_starttransfer}\n\nTotal Time:\t\t%{time_total}\n' -o /dev/null http://www.google.com
```

Output for Google.
```
Testing Website Response Time for :http://www.google.com/

Lookup Time:		    0.004151

Connect Time:		    0.079221

Pre-transfer Time:  	0.079257

Start-transfer Time:	0.199834

Total Time:		0.274493
```

Output for Stackbees
```
Testing Website Response Time for :http://stackbees.co

Lookup Time:    		0.004197

Connect Time:	    	0.174173

Pre-transfer Time:  	0.174238

Start-transfer Time:	0.346720

Total Time:		0.346767
```
 