---
title: About me
subtitle: 
comments: false
---
<center>

###  Bipindas 

</center>

## Major Goals

* Design, Deployment, Operating three Public Clouds in Saudi Arabia
* Migration of 35 Customers from one Public Cloud to another
* Onboarded 50+ Large customers in Public Cloud
* Customer Success Manager for 10+ Large accounts
* Team Lead,  Operations 
* Project Lead for three Public Cloud upgrades
 

## Recent Achievements

* Outstanding Employee for the  year 2024
* Outstanding Employee for the  year 2023
* Outstanding Employee for the  year 2022
* Outstanding Employee for the  year 2021
* Outstanding Employee for the  year 2020
* Outstanding Employee for the  year 2019

## Professional Certifications. 

* Comptia Security +
* Advanced Diploma in Asian Cyberlaw 
* Advanced Diploma in Cybercrime Prosecution & Defence 
* TOGAF 9 (L1 & L2)
* ITIL 4 Foundation
* Mirantis Certified Openstack Professional.
* ISO 27001 Lead Implementer.
* Certified Security Professional.
* PRINCE2 Foundation in Project Management.
* PRINCE2 Practitioner.



